 #-------------------------------------------------
#
# Project created by QtCreator 2013-07-17T19:29:54
#
#-------------------------------------------------

QT       += core gui widgets uitools

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GUI-Editor
TEMPLATE = app


SOURCES += main.cpp \
    mainwidget.cpp \
    mainview.cpp \
    mainscene.cpp \
    itemgrid.cpp \
    factory/ItemFactory.h \
    ui/propertiespanel.cpp \
    ui/toolwidget.cpp \
    ui/transformwidget.cpp \
    items/textwidget.cpp \
    items/textitem.cpp \
    items/SizeGripItem.cpp \
    items/panelitem.cpp \
    items/itembase.cpp \
    items/coloritem.cpp \
    items/buttonitem.cpp \
    items/gridlines.cpp \
    items/robot.cpp \
    document.cpp \
    items/documentpanel.cpp \
    items/layerpanel.cpp \
    ui/newdocumentdialog.cpp \
    ui/itemsbar.cpp \
    items/radiobuttonitem.cpp \
    items/slideritem.cpp \
    items/checkboxitem.cpp \
    items/sceneruler.cpp

DEFINES += CORE_LIBRARY

HEADERS  += \
    mainwidget.h \
    mainview.h \
    mainscene.h \
    itemgrid.h \
    ui/propertiespanel.h \
    ui/toolwidget.h \
    ui/transformwidget.h \
    items/textwidget.h \
    items/textitem.h \
    items/SizeGripItem.h \
    items/panelitem.h \
    items/itembase.h \
    items/coloritem.h \
    items/buttonitem.h \
    items/gridlines.h \
    items/robot.h \
    document.h \
    items/ItemHandler.h \
    interface/Exporter.h \
    items/documentpanel.h \
    items/layerpanel.h \
    ui/newdocumentdialog.h \
    ui/itemsbar.h \
    utils/Exporter.h \
    utils/Importer.h \
    items/radiobuttonitem.h \
    items/slideritem.h \
    items/checkboxitem.h \
    items/sceneruler.h \
    ui/QDRuler.h

FORMS    += \
    mainwindow.ui \
    ui/toolwidget.ui \
    ui/propertiespanel.ui \
    ui/transformwidget.ui \
    ui/newdocumentdialog.ui \
    ui/itemsbar.ui

RESOURCES += \
    rosource.qrc

OTHER_FILES += \
    resources/Doc.json \
    resources/default.json \
    images/circle.png \
    images/square.png \
    images/panel.png \
    images/onswitch.png \
    images/img.png \
    images/head.png \
    images/btn.png \
    images/add_item.png \
    images/3.png \
    images/txt.png \
    images/icons/zoom.png \
    images/icons/up.png \
    images/icons/unlocked.png \
    images/icons/txt.png \
    images/icons/trash.png \
    images/icons/square.png \
    images/icons/settings.png \
    images/icons/redo.png \
    images/icons/rectangle.png \
    images/icons/plus_cross.png \
    images/icons/panel.png \
    images/icons/onswitch.png \
    images/icons/movie.png \
    images/icons/minus_sign.png \
    images/icons/locked.png \
    images/icons/l_window.png \
    images/icons/l_vertical.png \
    images/icons/l_tabbed.png \
    images/icons/l_grid.png \
    images/icons/l_desc.png \
    images/icons/img.png \
    images/icons/head.png \
    images/icons/frame.png \
    images/icons/forward.png \
    images/icons/down.png \
    images/icons/delete.png \
    images/icons/circle.png \
    images/icons/checked.png \
    images/icons/cam.png \
    images/icons/btn.png \
    images/icons/back.png \
    images/icons/add_item.png
