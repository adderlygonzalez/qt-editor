#ifndef MAINVIEW_H
#define MAINVIEW_H

#include <QGridLayout>
#include <QVBoxLayout>
#include <QDebug>
#include <string>
#include <sstream>
#include "items/robot.h"
#include "items/gridlines.h"
#include "items/panelitem.h"
#include"mainscene.h"
#include"items/ItemHandler.h"
#include"document.h"
#include"ui/QDRuler.h"


/**
    Widget containing the scene of the application.
    Which all the elemtents are placed.
*/


class MainView: public QGraphicsView
{
  Q_OBJECT
public:
    MainView(QWidget* parent = 0);
    static MainView* instance(QWidget* parent = NULL);
   // void setUpScene(DocumentData &sceneData);

    QImage getSceneContent();
    DocumentPanel* getDocument(){return d_panel;}
    void setUpDocument(DocumentPanel* panel);
    void setUpRuler();
    void setCanZoom(bool what){canZoom = what;}
    MainScene* getMainScene(){return _scene;}
protected:
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);

    void wheelEvent(QWheelEvent *event);
    void mouseMoveEvent(QMouseEvent *event);
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);

    void dragEnterEvent(QDragEnterEvent *event);
    void dragMoveEvent(QDragMoveEvent *event);
    void dragLeaveEvent(QDragLeaveEvent *event);
    void dropEvent(QDropEvent *event);
    void resizeEvent(QResizeEvent *event);
private:
    bool canZoom;
    QDRuler* mHorzRuler;
    QDRuler* mVertRuler;
    static MainView* _instance ;
    PanelItem* mainPanel;
    DocumentPanel* d_panel;//
    MainScene *_scene;
    GridLines* _gridLines;
};

#endif // MAINVIEW_H
