#include "mainview.h"

MainView* MainView::_instance = NULL;

MainView* MainView::instance(QWidget* parent){
    if(!_instance){
        _instance = new MainView(parent);
    }
    return _instance;
}

QImage MainView::getSceneContent()
{
  QImage img;

  if(_scene)
    {
      //_scene->clearSelection();
      //_scene->setSceneRect(_scene->itemsBoundingRect());
      img =QImage(_scene->sceneRect().size().toSize(),QImage::Format_ARGB32);
      img.fill(Qt::transparent);

      QPainter painter;
      painter.begin(&img);
      _scene->render(&painter);
      painter.end();
      img.save("ttest.png");
    }
  return img;
}


MainView::MainView(QWidget* parent): QGraphicsView(parent),canZoom(false)
{
   setUpRuler();
   this->setAcceptDrops(true);

    _scene = new MainScene(this);
    _scene->setSceneRect(-1000, -1000, 10000,10000);
  //this->setV
    d_panel = new DocumentPanel(QRect(0, 0, 500, 500));

    LayerPanel* layer= new LayerPanel(QRect(0, 0, 400, 400));
    layer->setParentItem(d_panel);
    mainPanel = new PanelItem();
    mainPanel->setRect(QRect(0, 0, 300, 300));
    mainPanel->setPos(0,0);
    mainPanel->setMovable(false);
    mainPanel->setFlag(QGraphicsItem::ItemIsSelectable,false);
    mainPanel->setParentItem(layer);

    QGraphicsRectItem* rect = new QGraphicsRectItem(QRect(0,0,155,155));
    rect->setPos(150,150);
    rect->setBrush(QBrush(QColor(Qt::green)));
    rect->setFlag(QGraphicsItem::ItemIsMovable);
    rect->setParentItem(mainPanel);

    TextItem* textItem =new TextItem("Text",QRectF(0, 0, 100, 50));
    textItem->setPos(200,200);
    //_scene->addItem(textItem);

    ButtonItem* btn =  new ButtonItem("Btn",QRectF(0, 0, 100, 50));
    btn->setPos(150,150);

    PanelItem* panel = new PanelItem();
    panel->setRect(QRect(0, 0, 100, 150));
    panel->setPos(150,150);
   // btn->setParentItem(panel);
    panel->setParentItem(mainPanel);
    for (int i = 0; i < 10; ++i) {
        ColorItem *item = new ColorItem(NULL,QRectF(0, 0, 100, 100));
        item->setPos(::sin((i * 6.28) / 10.0) * 150,
                     ::cos((i * 6.28) / 10.0) * 150);
        item->setParentItem(mainPanel);
    }
    Robot *robot = new Robot;
    robot->setPos(0, -20);
    //robot->setParentItem(mainPanel);

    RadioButtonItem* radio = new RadioButtonItem(QRectF(0,0,20,20));
    radio->setPos(0,20);
    radio->setZValue(20);
    radio->setParentItem(mainPanel);

    CheckBoxItem* check = new CheckBoxItem(QRectF(0,0,20,20));
    check->setPos(0,30);
    check->setZValue(20);
    check->setParentItem(mainPanel);

    SliderItem* slider = new SliderItem(QRectF(0,0,80,20));
    slider->setPos(30,30);
    slider->setZValue(20);
    slider->setParentItem(mainPanel);

    _scene->addItem(d_panel);
    this->setScene(_scene);
    setAlignment(Qt::AlignTop);
    this->setOptimizationFlag(DontSavePainterState);
    this->setRenderHint(QPainter::Antialiasing);
    this->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    this->setDragMode(RubberBandDrag);
    //setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    //setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff );
 //   fitInView(QRect(-20,-20,200,200),Qt::KeepAspectRatioByExpanding);
    centerOn(d_panel);
}

void MainView::resizeEvent(QResizeEvent *event)
{
    Q_UNUSED(event);
  qDebug() << "resize";
   QGraphicsView::resizeEvent(event);

   //centerOn(d_panel);
}


void MainView::keyPressEvent(QKeyEvent *event)
{
  if(event->key() == Qt::Key_Shift){
      this->setDragMode(ScrollHandDrag);
    }
  if(event->key() == Qt::Key_Delete)
    {
      if(_scene){
           QList<QGraphicsItem*> selected = _scene->selectedItems();
           if(!selected.empty())
             {
               _scene->removeItem(selected.at(0));
             }
        }
    }
  QGraphicsView::keyPressEvent(event);
}

void MainView::keyReleaseEvent(QKeyEvent *event){
  if(event->key() == Qt::Key_Shift){
      this->setDragMode(RubberBandDrag);
    }
  QGraphicsView::keyReleaseEvent(event);
}
void MainView::wheelEvent(QWheelEvent *event)
{
  QPoint aDelta = event->angleDelta() / 8;

  int yDelta = aDelta.y();
  setTransformationAnchor(QGraphicsView::AnchorUnderMouse);

   // Scale the view / do the zoom
   double scaleFactor = 1.15;
   if(yDelta > 0) {
       // Zoom in
       //scale(scaleFactor, scaleFactor);
       setTransform(QTransform::fromScale(scaleFactor, scaleFactor));
   } else {
       // Zooming out
       //scale(1.0 / scaleFactor, 1.0 / scaleFactor);
       setTransform(QTransform::fromScale(1.0/scaleFactor, 1.0/scaleFactor));
   }

}

void MainView::mousePressEvent(QMouseEvent *event){
    QGraphicsView::mousePressEvent(event);
}

void MainView::mouseMoveEvent(QMouseEvent *event)
{
    QWidget* widget = QApplication::activeWindow();
    if(widget){
        QPointF scene_pos =mapToScene(event->pos().x(),event->pos().y());
        std::stringstream ss ;
        ss <<  " x = " << event->pos().x()  << "y = " << event->pos().y();
        ss <<  " s_x = " << scene_pos.x() << " s_y = " << event->pos().y();
        QString title = QString(ss.str().c_str());
        //qDebug() << title;
        widget->setWindowTitle(title);
    }


    QGraphicsView::mouseMoveEvent(event);
}
void MainView::mouseReleaseEvent(QMouseEvent *event)
{
    QGraphicsView::mouseReleaseEvent(event);
}

void MainView::dragEnterEvent(QDragEnterEvent *event)
{
   qDebug() << " mv enter ";
  // event->setAccepted(true);

    QGraphicsView::dragEnterEvent(event);
}
void MainView::dragMoveEvent(QDragMoveEvent *event)
{
    qDebug() << "  mv Drag move " ;
    /*
  //  qDebug() << "  Drag " ;
   if (event->mimeData()->hasFormat("application/x-dnditemdata")) {

       if (event->source()== this) {
           event->setDropAction(Qt::MoveAction);
           event->accept();
       } else {
           event->acceptProposedAction();
       }
   } else {
       event->ignore();
   }
   */
   // event->setAccepted(true);
    QGraphicsView::dragMoveEvent(event);
}
void MainView::dragLeaveEvent(QDragLeaveEvent *event)
{
    qDebug() << " mv drg leave" ;
    //event->setAccepted(true);
    QGraphicsView::dragLeaveEvent(event);
}

void MainView::dropEvent(QDropEvent *event)
{
    qDebug() << " mv  drop " ;
/*
    //qDebug() << " QpixMap drop " ;
    if (event->mimeData()->hasFormat("application/x-dnditemdata")) {
           QByteArray itemData = event->mimeData()->data("application/x-dnditemdata");
           QDataStream dataStream(&itemData, QIODevice::ReadOnly);

           QPixmap pixmap;
           dataStream >> pixmap ;

         //  qDebug() << " QpixMap " ;
           //Square* item = new Square;
           this->scene()->addPixmap(pixmap);
           event->setDropAction(Qt::MoveAction);
           event->accept();
           if (event->source() == this) {
           } else {
               event->acceptProposedAction();
           }
       } else {
           event->ignore();
       }*/

    QGraphicsView::dropEvent(event);
}


void MainView::setUpDocument(DocumentPanel *panel)
{
    if(d_panel && panel){
        _scene->removeItem(d_panel);
      }
    if(d_panel) {
        d_panel = panel;
        _scene->addItem(d_panel);
        _scene->setDocumentPanel(d_panel);
      }
}
void MainView::setUpRuler()
{
  setViewportMargins(RULER_BREADTH-10,RULER_BREADTH-10,0,0);
  QGridLayout* gridLayout = new QGridLayout();
  gridLayout->setSpacing(0);
  gridLayout->setMargin(0);

  mHorzRuler = new QDRuler(QDRuler::Horizontal);
  mVertRuler = new QDRuler(QDRuler::Vertical);

  mHorzRuler->setMouseTracking(true);

  QWidget* fake = new QWidget();
  fake->setBackgroundRole(QPalette::Window);
  fake->setFixedSize(RULER_BREADTH,RULER_BREADTH);
  gridLayout->addWidget(fake,0,0);
  gridLayout->addWidget(mHorzRuler,0,1);
  gridLayout->addWidget(mVertRuler,1,0);
  gridLayout->addWidget(this->viewport(),1,1);
  this->setLayout(gridLayout);
}


