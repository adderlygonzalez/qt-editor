#include <QtWidgets>
#include <mainwidget.h>
int main(int argc, char *argv[])
{
  QApplication app(argc,argv);
  qsrand(QTime(0,0,0).secsTo(QTime::currentTime()));


  MainWidget* main = MainWidget::instance();
  main->show();
  return app.exec();
}
