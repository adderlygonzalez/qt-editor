#include "document.h"

QString Document::untitle_doc =
QString(
  "{"
  "\"docName\": \"untitle_doc\", "
  "\"height\": 600,"
  "\"layers\": [         { "
  "\"boundingRect\": [                 0,                 0,                 800,                 600             ],  "
  "\"items\": [             ], "
  "\"opacity\": 1, "
  "\"parentId\": 1, "
  "\"pos\": [                0,                 0,                 0             ], "
  "\"rotation\": 0, "
  "\"scale\": 1, "
  "\"type\": 156, "
  "\"typename\": \"LayerPanel\"         }     ], "
  "\"type\": \"json\", "
  "\"width\": 800 }"
);
QString formBody = QString(
      " form  %1 \n"
      " { \n"
      "   autoWidth = true \n "
      "   autoHeight = true \n "
      "   theme = res/editor.theme \n "
      "  %2 \n"
      " } \n"
      );
QString elementBody = QString(
      " %1 %2 \n "
      " { \n"
      "  %3 \n"
      " } \n");/** 1 =  type ,2 = name(id) ,3 = contents */

Document::Document(QObject *parent) :
    QObject(parent)
{
    m_empty_doc = true;
}

void Document::createDocument(QString fileName, QString url)
{

}
QJsonObject* Document::create(QString filename, int width, int height, int documentType)
{
  QJsonObject newDoc = createEmpyDocument();
  if(!filename.isEmpty())
    newDoc.insert("docName",filename);//if no name is provided it keep 'untittled file'
  if(width > 0 && height > 0)
    {
      newDoc.insert("width",width);
      newDoc.insert("height",height);
      QJsonObject layer = newDoc.value("layers").toArray().at(0).toObject();
      QJsonArray arr;
      arr.append(QJsonValue(0));
      arr.append(QJsonValue(0));
      arr.append(QJsonValue(width));
      arr.append(QJsonValue(height));

      layer.insert("boundingRect",arr);

      QJsonArray layertmp;
      layertmp.append(layer);

      newDoc.insert("layers",layertmp);
    }
  return new QJsonObject(newDoc);
}

QJsonObject Document::createEmpyDocument()
{
  QJsonObject newDoc = validateAndLoadtext(untitle_doc);

  return newDoc;
}

void Document::loadFileDoc(QString file)
{
  emit loadedDoc(file);
}

QJsonObject* Document::parseDocument(const DocumentPanel *docPanel)
{
  QJsonObject* docData = new QJsonObject;
  docData->insert("docName",QString("mainDoc"));
  docData->insert("type",QString("json"));
  docData->insert("width" , docPanel->getRect().width());
  docData->insert("height" ,docPanel->getRect().height());

  QJsonArray layerArray;

  QList<QGraphicsItem*> childs = docPanel->childItems();
  QList<QGraphicsItem*>::Iterator it = childs.begin();
  for(;it != childs.end();it++)
  {
      QJsonObject* obj = parseItem((*it));
      layerArray.append(*obj);
  }
  docData->insert("layers",layerArray);
#ifdef DOC_IMPORT_DEBUG
  QJsonDocument *doc = new QJsonDocument(*docData);
  qDebug() << doc->toJson();
#endif
  return docData;
}

QJsonObject* Document::parseLayer(LayerPanel *layerItem)
{
  QJsonObject* layer = new QJsonObject;
  layer->insert("typename",layerItem->typeName());

  //parse childrens
  QList<QGraphicsItem*> childs = layerItem->childItems();
  QList<QGraphicsItem*>::Iterator it = childs.begin();

  QJsonArray items;
  for(;it!= childs.end();it++)
  {
      QJsonObject* tmp = parseItem((*it));
      items.append(*tmp);
  }
  layer->insert("items",items);
  return layer;
}

QJsonObject* Document::parseItem(const QGraphicsItem *item)
{
    QJsonObject* parsed = new QJsonObject;
    if(item)
    {
        QJsonObject* tmp = NULL ;
        int type = item->type();      
        
        parsed->insert("parentId",1);
        parsed->insert("type",type);
        parsed->insert("opacity",item->opacity());
        parsed->insert("rotation",item->rotation());
        parsed->insert("scale",item->scale());

        QPointF p = item->pos();
        QJsonArray i_pos;
        i_pos.append(p.x());//x
        i_pos.append(p.y());//y
        i_pos.append(item->zValue());//z
        parsed->insert("pos",i_pos);

        QRectF bRect = item->boundingRect();
        QJsonArray rect;
        rect.append(bRect.x());
        rect.append(bRect.y());
        rect.append(bRect.width());
        rect.append(bRect.height());
        parsed->insert("boundingRect",rect);

        
        switch(type)
        {
          case ItemBase::LayerType:
            {
                tmp = parseLayer(static_cast<LayerPanel*>(const_cast<QGraphicsItem*>(item)));
            }
            break;
          case ItemBase::PanelType:
            {            
                tmp = parsePanel(static_cast<PanelItem*>(const_cast<QGraphicsItem*>(item)));
            }
            break;
        case ItemBase::ButtonType:
          {            
            tmp = parseButton(static_cast<ButtonItem*>(const_cast<QGraphicsItem*>(item)));
          }
          break;
         case ItemBase::TextType:
          {            
            tmp = parseText(static_cast<TextItem*>(const_cast<QGraphicsItem*>(item)));
          }
          break;

          default:
              break;
        }
        
       mergeProperties(parsed,tmp);

    }
    return parsed;
}

QJsonObject* Document::parsePanel(PanelItem *panel)
{
    QJsonObject* pnl = new QJsonObject;
    pnl->insert("typename",panel->typeName());

    //parse childrens
    QList<QGraphicsItem*> childs = panel->childItems();
    QList<QGraphicsItem*>::Iterator it = childs.begin();

    QJsonArray items;
    for(;it!= childs.end();it++)
    {
        QJsonObject* tmp = parseItem((*it));
        items.append(*tmp);
    }
    pnl->insert("items",items);

    return pnl;
}
QJsonObject* Document::parseButton(ButtonItem *btn)
{
    QJsonObject* button = new QJsonObject;
    button->insert("typename",btn->typeName());
    button->insert("text",btn->getText());
    button->insert("fontsize",btn->getFontSize());
    button->insert("align",btn->getTextAlignment());
    return button;
}
QJsonObject* Document::parseText(TextItem *txt)
{
    QJsonObject* text = new QJsonObject;
    text->insert("typename",txt->typeName());
    text->insert("text",txt->getText());
    text->insert("fontsize",txt->getFontSize());
    text->insert("align",txt->getTextAlignment());
    return text;
}

void Document::mergeProperties(QJsonObject *to, QJsonObject *from)
{
    if(from != NULL && to != NULL)
    {
        QJsonObject::Iterator it = from->begin();
        for(;it != from->end();it++)
        {
            to->insert((it).key(),(it).value());
        }
    }else{
#ifdef DOC_IMPORT_DEBUG
        qDebug() << "mergeProperties NUll";
#endif
    }
}

QString Document::exportGamePlayForm(QJsonObject doc)
{

  /**
  "docName": "mainDoc",
  "height": 500,

    "type": "json",
    "width": 500
  */
  QString elements;
  QString docName = doc.find("docName").value().toString();
  qreal width = doc.find("width").value().toDouble();
  qreal height = doc.find("height").value().toDouble();
  QJsonArray layers = doc.find("layers").value().toArray();
  for(int n = 0;n < layers.size();n++)
    {
      qDebug() << "layer = " << n;
      QJsonObject layer = layers.at(n).toObject();
      QJsonArray items = layer.find("items").value().toArray();
      for(int  k = 0; k< items.size();k++)
        {
          qDebug() << "item = " <<k;
          QJsonObject item = items.at(k).toObject();
          elements += exportGamePlayItem(item);
          qDebug() << elements;
        }
    }

  return formBody.arg(docName).arg(elements);
}

QString Document::exportGamePlayItem(QJsonObject obj/*item*/)
{
  QString data;
  QString typeName;

  if(obj.contains("type"))
    {
      int parentId = obj.contains("parentId") ? obj.value("parentId").toDouble(0):0;
      qreal opacity = obj.contains("opacity") ? obj.value("opacity").toDouble(0):0;
      qreal rotation = obj.contains("rotation") ? obj.value("rotation").toDouble(0):0;
      qreal scale = obj.contains("scale") ? obj.value("scale").toDouble(0):0;
      qreal zval;
      QRectF rect;
      QPointF pos;

      if(obj.contains("boundingRect")){
          QJsonArray bRect = obj.value("boundingRect").toArray();
          rect = QRectF(bRect.at(0).toDouble(0),bRect.at(1).toDouble(0),
                        bRect.at(2).toDouble(0),bRect.at(3).toDouble(0));

        }
      if(obj.contains("pos")){
          QJsonArray posArr = obj.value("pos").toArray();
          pos = QPointF(posArr.at(0).toDouble(0),posArr.at(1).toDouble(0));
          zval = posArr.at(2).toDouble(0);
        }

      int type = obj.value("type").toDouble(0);
        #ifdef  DOC_IMPORT_DEBUG
      qDebug() << "Item type " << type;
      qDebug() << " \ttypename" << obj.value("typename").toString();
      qDebug() << " \trect " << rect.x() << " "<< rect.y() << " " << rect.width() << " "<< rect.height() << " ";
      qDebug() << " \topacity " << opacity;
      qDebug() << " \trotation "<< rotation;
      qDebug() << " \tscale "<<scale;
      qDebug() << " \tzval "<< zval;
      qDebug() << " \tpos x " << pos.x() << " y "<<pos.y();
        #endif
      QString items;
      switch(type)
        {
        case ItemBase::PanelType:{

            if(obj.contains("items"))
              {
                items = exportGamePlayContainer(obj);
              }
            typeName = "container";
          }break;
        case ItemBase::LayerType:{
            typeName = "layer";
          }break;
        case ItemBase::TextType:{
            typeName = "text";
            if(obj.contains("text")){
                items += " text = "+obj.find("text").value().toString()+"\n";
              }
          }break;
        case ItemBase::ColorType:{
            typeName = "color";
          }break;
        case ItemBase::ButtonType:{
            typeName = "button";
          }break;

        default:
          return QString();
          break;
        }
     //   data += "\n width = "+rect.width();
     //   data += "\n heigth = "+rect.height();
     //   data += "\n position = "+pos.x()+","+pos.y();
       // data += QString("\n width = %1 \n height = %2 \n position = %3,%4 \n ").arg(rect.width()).arg(rect.height()).arg(pos.x()).arg(pos.y());

        /*
            item->setRect(rect);
            item->setOpacity(opacity);
            item->setRotation(rotation);
            item->setScale(scale);
            item->setPos(pos);
            item->setZValue(zval);
        */
        QString tmpData;
        tmpData += QString(
              " \n width = %1 \n"
              "\n heigth = %2 \n"
              "\n position = %3 , %4 \n "
              ).arg(rect.width()).arg(rect.height()).arg(pos.x()).arg(pos.y());

        if(!items.isEmpty())
        {
          tmpData += items;
        }
        data += elementBody.arg(typeName).arg(typeName).arg(tmpData);

    }
  return data;
}

QString Document::exportGamePlayContainer(QJsonObject &doc)
{
  QString elements;
  QJsonArray arrElements = doc.find("items").value().toArray();

  for(int n = 0; n < arrElements.size(); n++)
    {
        elements += exportGamePlayItem(arrElements.at(n).toObject());
    }
  return elements;
}

DocumentPanel* Document::importFromJson(const char *filename)
{
  DocumentPanel* item = NULL;
  QJsonObject* jobj = validateAndLoad(filename);

  validateAndLoadtext(untitle_doc);
#ifdef DOC_IMPORT_DEBUG
  QJsonDocument doc;
  doc.setObject(*jobj);
  qDebug() << "Loaded Doc ";
  qDebug() << doc.toJson();
#endif
  validateAndLoadtext(untitle_doc);
  if(jobj){
     item =  loadDocument(jobj);
    }
  return item;
}

DocumentPanel* Document::createNew(QString filename, int width, int height, int documentType)
{
  QJsonObject* dobj = create(filename,width,height,documentType);
  DocumentPanel* dPanel = loadDocument(dobj);
  return dPanel;
}

DocumentPanel* Document::loadDocument(QJsonObject *json)
{
  DocumentPanel* d_panel = new DocumentPanel;

  if(json == NULL) return NULL;
  int width = json->value("width").toDouble(0);
  int height = json->value("height").toDouble(0);
  QRectF mainRect = QRect(0,0,width,height);
  d_panel->setRect(mainRect);

  //check if there is position on
  if(json->contains("pos") ){
      QJsonArray pos = json->value("pos").toArray();
      d_panel->setPos(pos.at(0).toDouble(0),pos.at(1).toDouble(0));
      d_panel->setZValue(pos.at(2).toDouble(0));
    }

  if(json->contains("layers")){
      QJsonArray layers = json->take("layers").toArray();
      QJsonArray tmpLayer(layers);
      QJsonArray::Iterator it = tmpLayer.begin();
      for(; it != layers.end();it++)
        {
          QJsonObject arrObj = (*it).toObject();
          qDebug() << "layerSize " << layers.size();
#ifdef DOC_IMPORT_DEBUG
          m_json_doc.setObject(arrObj);
          qDebug() << " ArrElement";
          qDebug() << m_json_doc.toJson();
          qDebug() << " ArrElement END";
#endif
          ItemBase* loaded = loadItem(arrObj);
          if(loaded){
              loaded->setParentItem(d_panel);
            }
        }
    }

  d_panel->setFlag(QGraphicsItem::ItemIsSelectable,false);
  d_panel->setMovable(false);
  return d_panel;
}


ItemBase* Document::loadItem(QJsonObject obj)
{
  ItemBase* item = NULL;
  if(obj.contains("type"))
    {
      int parentId = obj.contains("parentId") ? obj.value("parentId").toDouble(0):0;
      qreal opacity = obj.contains("opacity") ? obj.value("opacity").toDouble(0):0;
      qreal rotation = obj.contains("rotation") ? obj.value("rotation").toDouble(0):0;
      qreal scale = obj.contains("scale") ? obj.value("scale").toDouble(0):0;
      qreal zval;
      QRectF rect;
      QPointF pos;

      if(obj.contains("boundingRect")){
          QJsonArray bRect = obj.value("boundingRect").toArray();
          rect = QRectF(bRect.at(0).toDouble(0),bRect.at(1).toDouble(0),
                        bRect.at(2).toDouble(0),bRect.at(3).toDouble(0));

        }
      if(obj.contains("pos")){
          QJsonArray posArr = obj.value("pos").toArray();
          pos = QPointF(posArr.at(0).toDouble(0),posArr.at(1).toDouble(0));
          zval = posArr.at(2).toDouble(0);
        }

      int type = obj.value("type").toDouble(0);
        #ifdef  DOC_IMPORT_DEBUG
      qDebug() << "Item type " << type;
      qDebug() << " \ttypename" << obj.value("typename").toString();
      qDebug() << " \trect " << rect.x() << " "<< rect.y() << " " << rect.width() << " "<< rect.height() << " ";
      qDebug() << " \topacity " << opacity;
      qDebug() << " \trotation "<< rotation;
      qDebug() << " \tscale "<<scale;
      qDebug() << " \tzval "<< zval;
      qDebug() << " \tpos x " << pos.x() << " y "<<pos.y();
        #endif

      switch(type)
        {
        case ItemBase::PanelType:{
            item = loadPanel(obj);
          }break;
        case ItemBase::LayerType:{
            item = loadLayer(obj);
          }break;
        case ItemBase::TextType:{
            item = loadText(obj);
          }break;
        case ItemBase::ColorType:{
            item = loadColorItem(obj);
          }break;
        case ItemBase::ButtonType:{
            item = loadButton(obj);
          }break;
        }

      if(item){
          if(!rect.isEmpty())
            item->setRect(rect);
          else item->setRect(QRect(0,0,100,100));
            item->setOpacity(opacity);
            item->setRotation(rotation);
            item->setScale(scale);
            item->setPos(pos);
            item->setZValue(zval);
        }
    }
  return item;
}

ItemBase* Document::loadPanel(QJsonObject &obj)
{
  PanelItem* panel = new PanelItem();

  if(obj.contains("items"))
    {
      QJsonArray arr = obj.value("items").toArray();
      for(int n = 0;n < arr.size();n++)
        {
          ItemBase* item = NULL;
          item = loadItem(arr.at(n).toObject());
          if(item){
              item->setParentItem(panel);
            }
        }

    }
  return panel;
}

ItemBase* Document::loadText(QJsonObject &obj)
{
  QString text = obj.contains("text") ? obj.value("text").toString() :"Text Val";
  TextItem* textItem = new TextItem(text);

  if(obj.contains("fontsize")){
      textItem->setFontSize(obj.value("fontsize").toDouble());
    }
  if(obj.contains("align")){
      textItem->positionText(obj.value("align").toDouble());
    }
  if(obj.contains("typename")){
    }
  return textItem;
}

ItemBase* Document::loadButton(QJsonObject &obj)
{
  QString text = obj.contains("text") ? obj.value("text").toString() :"Text Val";
  ButtonItem* btn = new ButtonItem(text);

  if(obj.contains("fontsize")){
      btn->setFontSize(obj.value("fontsize").toDouble());
    }
  if(obj.contains("align")){
      btn->positionText(obj.value("align").toDouble());
    }
  if(obj.contains("typename")){
    }
  return btn;
}

ItemBase* Document::loadColorItem(QJsonObject &obj)
{
  ColorItem* citem = new ColorItem;
  return citem;
}

ItemBase* Document::loadLayer(QJsonObject &obj)
{
  LayerPanel* layer = new LayerPanel;
  layer->setFlag(QGraphicsItem::ItemIsSelectable,false);
  if(obj.contains("items"))
    {
      QJsonArray arr = obj.value("items").toArray();
      for(int n = 0;n < arr.size();n++)
        {
          ItemBase* item = NULL;
          item = loadItem(arr.at(n).toObject());
          if(item){
              item->setParentItem(layer);
            }
        }

    }
  return layer;
}

QJsonObject* Document::validateDocument(const char *filename)
{
  QJsonObject* json = NULL;
  if(filename == NULL && !m_json_doc.isNull() && !m_json_doc.isEmpty())
    {
      json = new QJsonObject( m_json_doc.object());
    }
  return json;
}

QJsonParseError Document::validateJsonFile(const char *filename)
{
    QJsonParseError parser;
    QFile jsonFile(filename);
    QByteArray b_arr;

    if(jsonFile.open(QIODevice::ReadOnly | QIODevice::Text)){
        b_arr = jsonFile.readAll();
    }
    if(jsonFile.isOpen())
      jsonFile.close();

    QJsonDocument::fromJson(b_arr,&parser);

    return parser;
}

QJsonObject Document::validateAndLoadtext(QString data)
{
  QJsonParseError parser;
  QJsonObject obj ;
  int error ;


  QJsonDocument tmp_doc = QJsonDocument::fromJson(data.toUtf8(),&parser);
#ifdef DOC_IMPORT_DEBUG
  qDebug() << " text loaded "<< tmp_doc.toJson();
#endif
  obj =  tmp_doc.object();

  return obj;
}


QJsonObject* Document::validateAndLoad(const char *filename)
{
  QJsonParseError parser;
  QFile jsonFile(filename);
  QByteArray b_arr;
  QJsonObject* obj = NULL;


  if(jsonFile.open(QIODevice::ReadOnly)){
      b_arr = jsonFile.readAll();
  }

  m_json_doc =  QJsonDocument::fromJson(b_arr,&parser);

  m_empty_doc = ! (parser.error == QJsonParseError::NoError);

  if(!m_empty_doc){
#ifdef DOC_IMPORT_DEBUG
      qDebug() << "First Parse DOc";
      qDebug() << m_json_doc.toJson();
#endif
      obj = new QJsonObject(m_json_doc.object());
  }

  return obj;
}

void Document::printDocument()
{
    if(!m_json_doc.isNull() && !m_json_doc.isEmpty()){
        qDebug() << m_json_doc.toJson();
    }
}

void Document::printObject(QJsonObject obj, QString id )
{
  QJsonDocument tmp_doc;
  tmp_doc.setObject(obj);
  qDebug() << id << tmp_doc.toJson();
}

void Document::saveToFile(QJsonObject *obj, const QString file)
{
  QFile f_info(file);
  if(f_info.open(QFile::ReadWrite)){
      QJsonDocument doc(*obj);

      QTextStream o_stream;
      o_stream.setDevice(&f_info);
      o_stream << doc.toJson();
    }
  if(f_info.isOpen()) f_info.close();

}


void Document::fileNotFoundNotification(QString name)
{

}
