#ifndef EXPORTER_H
#define EXPORTER_H

struct DocumentData;
class DocumentExporter
{
public:
    virtual void write(DocumentData &) = 0;
};


#endif // EXPORTER_H
