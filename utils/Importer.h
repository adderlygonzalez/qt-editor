#ifndef IMPORTER_H
#define IMPORTER_H

/**
    Interface for loading any doc. (with specific format)
*/

class DocumentImporter
{
public:
    virtual void load() = 0;


};
#endif // IMPORTER_H
