#include "itemgrid.h"

ItemGrid::ItemGrid(QWidget* parent):QGraphicsView(parent)
{
    init();
    loadItems();
    setUpView();
}
/**
*/
void ItemGrid::init()
{    
    scene = new QGraphicsScene(this);
    scene->setSceneRect(0, 0, 200, 200);
    this->setScene(scene);
    this->setRenderHint(QPainter::Antialiasing);
    this->setViewportUpdateMode(QGraphicsView::BoundingRectViewportUpdate);
    //this->setBackgroundBrush(QColor(230, 200, 167));
}

/**
    TODO: read available elements from a jsonFile.
*/
void ItemGrid::loadItems()
{
    ColorItem* c_item  = new ColorItem(NULL,QRectF(0, 0, 60, 60));
    c_item->setAllowCopy(true);

    TextItem* textitem = new TextItem("TextInput",QRectF(0,0,60,60));
    textitem->setAllowCopy(true);
    textitem->setTransformable(false);
    textitem->centerText();

    ButtonItem* btnitem = new ButtonItem("Button Item",QRectF(0,0,100,60));
    btnitem->setAllowCopy(true);


    QGraphicsGridLayout *grid = new QGraphicsGridLayout;
    grid->setSpacing(15);
    //grid->addItem(linear,1,1);
    item_grid = new QGraphicsWidget;
    item_grid->setLayout(grid);
   // item_grid->setPos(mapToScene(scene->sceneRect().x(),scene->sceneRect().y()));
    item_grid->setPos(0,0);
    scene->addItem(item_grid);
    addItemLayout(c_item);
    addItemLayout(textitem);
    addItemLayout(btnitem);
}
/**
 Add item to GridLayout.
*/
void ItemGrid::addItemLayout(QGraphicsLayoutItem *item,int row,int column)
{
    QGraphicsGridLayout* g_layout = static_cast<QGraphicsGridLayout*>(item_grid->layout());
    if(g_layout)
    {
        g_layout->addItem(item,g_layout->rowCount()+1,column);
    }
}

/**
TODO:SetUp correctly the width of this widget
*/
void ItemGrid::setUpView()
{
    if(scene)
    {
        QRectF itemsSize = scene->itemsBoundingRect();
        itemsSize.setWidth(itemsSize.width() + 10);

        scene->setSceneRect(itemsSize);
        //this->setMaximumWidth(scene->itemsBoundingRect().width());
        this->setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Expanding);
    }else{

    }
}


