#ifndef ITEM_FACTORY
#define ITEM_FACTORY

typedef struct{
    static const unsigned COLOR_ITEM = 0;
    static const unsigned SQUARE_ITEM = 1;
}itemTypes;

template<class T,class I>
T* createItem(I const& type)
{
    T* item ;
    if(type == itemTypes::COLOR_ITEM){
        item =  new T();
    }else{
        item = NULL;
    }
    return item;
}


#endif
