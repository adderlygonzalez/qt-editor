#ifndef DOCUMENT_H
#define DOCUMENT_H

#include <QtCore>
#include<QFile>
#include<QJsonDocument>
#include<QFileDialog>
#include"items/panelitem.h"
#include"items/layerpanel.h"
#include"items/documentpanel.h"
/**

This class will be use to create Document behavior and properties,
to easily manage it.

Description of the Document Structure, which will be representing the contents and
structure of the scene and the items inside of it.

{
   "docName":"",
   "type":"JSON-COCOS2D",
   "width":154,
   "height":15,
   "layers":{
   }
}


"layer":{
    "pos":[x,y,z],
    "height":"inherited from doc",
    "width":"inherited from doc",
    "id":15,
    "name":"background",
    "opacity":0.5,
    "z_val":02,
    "items":[
       {
        "typename":"textitem",
        "type":150,
        "pos":[x,y,z],
        "boundingRect":[0,0,0,0],
        "opacity":0.5,
        "rotation":0,
        "scale":1.0
       }
    ]

}
*/

#define EMPTY_DOC "Empty Document"
#define DEFAULT_DOC_WIDTH 1024
#define DEFAULT_DOC_HEIGHT 768
#define DOC_IMPORT_DEBUG 1


class Document : public QObject
{
    Q_OBJECT
public:
    explicit Document(QObject *parent = 0);
    void createDocument(QString fileName = QString(),QString url = QString());/** create a doc*/
    QJsonObject* create(QString filename,int width, int height, int documentType = 0);
    QJsonObject createEmpyDocument();

    //export
    /** Receive the main Panel Containing all elements*/
    /** Create a json with common properties of items.
        and handle the item to a more specialize method.
    */
    QJsonObject* parseDocument(const DocumentPanel* docPanel);
    QJsonObject* parseLayer(LayerPanel* layer);
    QJsonObject* parseItem(const QGraphicsItem* item);
    QJsonObject* parsePanel(PanelItem* panel);/** panel containing other items. */
    QJsonObject* parseButton(ButtonItem* btn);
    QJsonObject* parseText(TextItem* text);
    void mergeProperties(QJsonObject* to,QJsonObject* from);


    //GAMEPLAY3D
    QString exportGamePlayForm(QJsonObject doc);
    QString exportGamePlayItem(QJsonObject obj);
    QString exportGamePlayContainer(QJsonObject &obj);

    //import

    DocumentPanel* importFromJson(const char* filename);
    DocumentPanel* createNew(QString filename,int width, int height, int documentType);
    DocumentPanel* loadDocument(QJsonObject* json);
    ItemBase* loadItem(QJsonObject obj);
    ItemBase* loadPanel(QJsonObject& obj);
    ItemBase* loadText(QJsonObject& obj);
    ItemBase* loadButton(QJsonObject& obj);
    ItemBase* loadColorItem(QJsonObject& obj);
    ItemBase* loadLayer(QJsonObject& obj);

    QJsonObject* validateDocument(const char* filename = NULL);
    QJsonParseError validateJsonFile(const char* filename);
    QJsonObject validateAndLoadtext(QString data);
    QJsonObject* validateAndLoad(const char* filename);
    void printDocument();
    void printObject(QJsonObject obj,QString id = QString("objprint"));//print jsonobject
    void saveToFile(QJsonObject* obj,const QString );

    void setEmpty(bool which){ this->m_empty_doc = which;}
    bool emptyDoc(){return this->m_empty_doc;}

    static QString untitle_doc;
signals:
    void fileNotFound(QString name);
    void loadedDoc(QString);

public slots:
    void loadFileDoc(QString);
    void fileNotFoundNotification(QString name);

private:
    QJsonDocument m_json_doc;
    QJsonDocument m_tmp_doc;
    bool m_empty_doc;

};

#endif // DOCUMENT_H
