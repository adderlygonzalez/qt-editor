#include "radiobuttonitem.h"

RadioButtonItem::RadioButtonItem(QRectF rect ,QGraphicsItem* parent):ItemBase(parent,rect)
{
}

void RadioButtonItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  QPen pen(QColor(Qt::black));
  painter->setPen(pen);

  painter->drawEllipse( _rect);
  if(isSelected())
    {
      painter->setBrush(QBrush(QColor(Qt::gray)));
    }else{
      painter->setBrush(QBrush(QColor(Qt::lightGray)));
    }
    painter->drawEllipse(QRectF(3,3,_rect.width() - 5,_rect.height() - 5));
}
