#include "sceneruler.h"

SceneRuler::SceneRuler(QRectF rect,QGraphicsItem* parent):QGraphicsRectItem(rect,parent)
{
}

void SceneRuler::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  QRect trect = QRect(0,0,20,1000);
  int x = pos().x();
  int y = pos().y();
  painter->setBrush(QBrush(Qt::white));
  painter->drawRect(trect);
  const int steps = 100;
  int counter = 0;
  int jump = 0;

  QPen pen(QColor(Qt::red));
  painter->setPen(pen);
  for(int n = 0;n < 1000;n++,counter++,jump++)
    {
      if(counter == 100)
        {
          painter->drawLine(x,y+n,20,y+n);
          counter = 0;
        }
      if(jump == 10){
          painter->drawLine(x+10,y+n,20,y+n);
          jump = 0;
        }
    }
}
