#include "gridlines.h"
#include <QColor>

GridLines::GridLines( int w, int h ,int space ) : QGraphicsItem(),
            _width(w), _height(h), _space(space)
{
    GraphicsItemFlags flags;
    flags&= QGraphicsItem::ItemIsFocusable;
    flags&= QGraphicsItem::ItemIsSelectable;
    setFlags(flags);
}

void GridLines::paint (QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)
    QColor c (Qt::black);
    painter->setPen(c);
    QPen pen = painter->pen();
    pen.setWidthF(0.2);
    painter->setPen(pen);
    for (int y= 0; y < _height; y+=_space)
    {
        painter->drawLine(0,y, _width, y);
    }

    for (int x= 0; x < _width; x+=_space)
    {
        painter->drawLine(x,0, x, _height);
    }
}

QRectF GridLines::boundingRect() const
{
    return QRectF (0,0,_width,_height);
}

void GridLines::handleWindowSizeChanged(int w, int h)
{
    _width = w;
    _height = h;
}

void GridLines::mousePressEvent ( QGraphicsSceneMouseEvent * event )
{
    QGraphicsItem::mousePressEvent(event);
}

void GridLines::mouseMoveEvent ( QGraphicsSceneMouseEvent * event )
{
    QGraphicsItem::mouseMoveEvent(event);
}
