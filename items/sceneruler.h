#ifndef SCENERULER_H
#define SCENERULER_H

#include <QtWidgets>

class SceneRuler : public QGraphicsRectItem
{
public:
  SceneRuler(QRectF rect,QGraphicsItem *parent = NULL);
protected:
  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
};

#endif // SCENERULER_H
