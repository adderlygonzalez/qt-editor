#ifndef SLIDERITEM_H
#define SLIDERITEM_H
#include"itembase.h"

class SliderItem: public ItemBase
{
  class SliderGrabItem : public QGraphicsRectItem
  {
  public:
    SliderGrabItem(QRectF rect = QRectF(0,0,5,15) ,QGraphicsItem* parent = NULL);
  protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
  };
public:
  enum{Type=SliderType};
  int type() const { return Type;}
  SliderItem(QRectF rect = QRectF(0,0,60,20),QGraphicsItem* parent = NULL);
protected:
  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
private:
  SliderGrabItem* m_grab;
};

#endif // SLIDERITEM_H
