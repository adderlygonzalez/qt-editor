#include "textwidget.h"

TextWidget::TextWidget(QWidget* parent): QTextEdit(parent)
{
    init();
}
void TextWidget::init()
{

}

void TextWidget::keyReleaseEvent(QKeyEvent *event)
{
    if(event->key() == Qt::Key_Escape){
        this->setVisible(false);
    }
    QTextEdit::keyReleaseEvent(event);
}
