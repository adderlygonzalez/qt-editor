/****************************************************************************
**
** Copyright (C) 2010 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial Usage
** Licensees holding valid Qt Commercial licenses may use this file in
** accordance with the Qt Commercial License Agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Nokia.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
** $QT_END_LICENSE$
**
****************************************************************************/

#include "coloritem.h"

ColorItem::ColorItem(QGraphicsItem* parent,QRectF rect):ItemBase(parent,rect),  color(qrand() % 256, qrand() % 256, qrand() % 256)
{
  setToolTip(QString("QColor(%1, %2, %3)\n%4")
             .arg(color.red()).arg(color.green()).arg(color.blue())
             .arg("Click and drag this color onto the robot!"));
  setCursor(Qt::OpenHandCursor);
  GraphicsItemFlags flags;
  flags|= QGraphicsItem::ItemIsMovable | QGraphicsItem::ItemIsSelectable;
  this->setFlags(flags);

 }

QRectF ColorItem::boundingRect() const
{
    if(!_rect.isNull()){
        return _rect;
    }
  return QRectF(-15.5, -15.5, 34, 34);
}
QPainterPath ColorItem::shape() const
{
  QPainterPath path;
  path.addEllipse(boundingRect());
  return path;
}
void ColorItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  Q_UNUSED(option);
  Q_UNUSED(widget);
  painter->setPen(Qt::NoPen);
  painter->setPen(QPen(Qt::black, 1));
  if(isSelected()){
      painter->setBrush(QBrush(Qt::red));
  }else{
      painter->setBrush(QBrush(color));
  }
    painter->drawPath(shape());
}
void ColorItem::updateItemContents()
{
    update(boundingRect());
}

void ColorItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
  if (event->button() != Qt::LeftButton) {
      event->ignore();
      return;
    }
    setCursor(Qt::ClosedHandCursor);
}

void ColorItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
  if (QLineF(event->screenPos(), event->buttonDownScreenPos(Qt::LeftButton))
      .length() < QApplication::startDragDistance()) {
      return;
    }
  if(allowCopy){
      QDrag *drag = new QDrag(event->widget());
      QMimeData *mime = new QMimeData;
      drag->setMimeData(mime);

      static int n = 0;
      if (n++ > 2 && (qrand() % 3) == 0) {
          QImage image(":/images/icons/head.png");
          mime->setImageData(image);

          drag->setPixmap(QPixmap::fromImage(image).scaled(30, 40));
          drag->setHotSpot(QPoint(15, 30));
        } else {
          mime->setColorData(color);
          mime->setText(QString("#%1%2%3")
                        .arg(color.red(), 2, 16, QLatin1Char('0'))
                        .arg(color.green(), 2, 16, QLatin1Char('0'))
                        .arg(color.blue(), 2, 16, QLatin1Char('0')));
          mime->setProperty("item_id",ColorItem::Type);

          QPixmap pixmap(34, 34);
          pixmap.fill(Qt::white);

          QPainter painter(&pixmap);
          painter.translate(15, 15);
          painter.setRenderHint(QPainter::Antialiasing);
          paint(&painter, 0, 0);
          painter.end();

          pixmap.setMask(pixmap.createHeuristicMask());

          drag->setPixmap(pixmap);
          drag->setHotSpot(QPoint(15, 20));
        }

      drag->exec();
      setCursor(Qt::OpenHandCursor);
  }else{
      QGraphicsWidget::mouseMoveEvent(event);
  }
}

void ColorItem::mouseReleaseEvent(QGraphicsSceneMouseEvent * event)
{
  setCursor(Qt::OpenHandCursor);
  QGraphicsWidget::mouseReleaseEvent(event);
}
