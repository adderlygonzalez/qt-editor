#include "textitem.h"

TextItem::TextItem(QString text ,QRectF rect,QGraphicsItem* parent): ItemBase(parent,rect)
{
    m_text = text;//init text
    m_text_initialized = false;
    init();
}

void TextItem::init()
{
    text = new QGraphicsSimpleTextItem(m_text,this);
    text->setFlag(ItemIsMovable,false);
    text->setFlag(ItemIsSelectable,false);
    connect(this,&TextItem::selectedChanged,this,&TextItem::hideTextWidget);    
    //connect(this,&TextItem::textChanged,this,&TextItem::centerText);
    positionText(AlignCenter);
}
void TextItem::initTextWidget()
{
    if(!m_text_initialized){
        QGraphicsScene* _scene =  scene();
        text_widget = new TextWidget();
        m_proxy = _scene->addWidget(text_widget);
        m_proxy->setVisible(false);
        connect(text_widget,&TextWidget::textChanged,this,&TextItem::getWidgetText);
        m_text_initialized = true;
    }
}
void TextItem::centerText(int align)
{
    positionText(align);
}

void TextItem::center(QVariant v)
{
  centerText(AlignCenter);
}
void TextItem::positionText(int align)
{
  //TODO: complete alignment
    ALIGNED = align;
    QPointF tpos;
    switch(align)
    {
      case AlignTop:{
           tpos = _rect.topRight() ;
          tpos.setX(tpos.x() - (this->text->boundingRect().width() / 2));
          tpos.setY(tpos.y() - (this->text->boundingRect().height() / 2));
        }
        break;
      case AlignCenter:{
           tpos = _rect.center() ;
          tpos.setX(tpos.x() - (this->text->boundingRect().width() / 2));
          tpos.setY(tpos.y() - (this->text->boundingRect().height() / 2));
        }
        break;
      default:
        break;
    }
    this->text->setPos(tpos);
}
QString TextItem::getText()
{
    if(text){
        return text->text();
    }else{
        return NULL;
    }
}

qreal TextItem::getFontSize()
{
    if(text){
        return text->font().pointSizeF();
    }
    return -1;
}
void TextItem::setFontSize(qreal size){
    if(text){
        QFont font = text->font();
        font.setPointSizeF(size);
    }
}

void TextItem::setText(const QString txt){
    if(text){
        text->setText(txt);
    }
}
void TextItem::hideTextWidget()
{
    if(m_text_initialized){
        if(text_widget)
            text_widget->setVisible(false);
    }
}

void TextItem::getWidgetText()
{
    if(text_widget){
        setText(text_widget->toPlainText());
    }
}
void TextItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  QRectF rect = getRect();
  QPen pen(QColor(Qt::red));
  painter->setPen(pen);

  painter->drawLine(rect.topLeft(),rect.topRight());
  painter->drawLine(rect.topRight(),rect.bottomRight());
  painter->drawLine(rect.bottomRight(),rect.bottomLeft());
  painter->drawLine(rect.bottomLeft(),rect.topLeft());

}

void TextItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
  if (event->button() != Qt::LeftButton) {
      event->ignore();
      return;
    }
    setCursor(Qt::ClosedHandCursor);
}
void TextItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if(!m_text_initialized)
        initTextWidget();

    if(allowCopy){
        QDrag *drag = new QDrag(event->widget());
        QMimeData *mime = new QMimeData;
        drag->setMimeData(mime);

        static int n = 0;
        if (n++ > 2 && (qrand() % 3) == 0) {
            QImage image(":/images/head.png");
            mime->setImageData(image);

            drag->setPixmap(QPixmap::fromImage(image).scaled(30, 40));
            drag->setHotSpot(QPoint(15, 30));
          } else {
         //   mime->setColorData(color);
            mime->setProperty("item_id",TextItem::Type);

            QPixmap pixmap(34, 34);
            pixmap.fill(Qt::white);

            QPainter painter(&pixmap);
            painter.translate(15, 15);
            painter.setRenderHint(QPainter::Antialiasing);
            paint(&painter, 0, 0);
            painter.end();

            pixmap.setMask(pixmap.createHeuristicMask());

            drag->setPixmap(pixmap);
            drag->setHotSpot(QPoint(15, 20));
          }

        drag->exec();
        setCursor(Qt::OpenHandCursor);
    }else{
        QGraphicsWidget::mouseMoveEvent(event);
    }
    QGraphicsItem::mouseMoveEvent(event);
}

void TextItem::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
    if(m_text_initialized){
        m_proxy->setPos(mapToScene(event->pos()));
        m_proxy->setVisible(true);
    }
    QGraphicsItem::mouseDoubleClickEvent(event);
}

