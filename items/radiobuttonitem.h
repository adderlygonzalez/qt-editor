#ifndef RADIOBUTTONITEM_H
#define RADIOBUTTONITEM_H
#include"itembase.h"

class RadioButtonItem: public ItemBase
{
public:
  enum{Type=RadioButtonType};
  int type() const { return Type;}
  RadioButtonItem(QRectF rect = QRectF(0,0,10,10),QGraphicsItem* parent = NULL);
protected:
  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
private:
  bool checked;
};

#endif // RADIOBUTTONITEM_H
