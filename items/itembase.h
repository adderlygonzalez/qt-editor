#ifndef ITEMBASE_H
#define ITEMBASE_H
#include<QtWidgets>
#include"factory/ItemFactory.h"
#include"SizeGripItem.h"
struct DEFAULT_ITEM_SIZE{
   static const qreal WIDTH=100,HEIGHT= 100;
};
#define MAIN_PANEL "main_p"
/**
    This is the base class for the items that ara gonna be used in the editor.
    This by itself provide scale behavior, movable properties and some signals/slot
    for modifying properties pos.

    Dynamic properties:
    id,
    exportable
**/

class ItemBase: public QGraphicsWidget
{
    Q_OBJECT
public:
    enum
    {
      PanelType=152,ColorType=153,ButtonType=154,TextType=155,LayerType =156,DocumentPanelType=157,
      CheckBoxType=158,RadioButtonType=159,SliderType=160,LabelType=161
    };
    ItemBase(QGraphicsItem* parent= NULL,QRectF rect = QRectF());
    ~ItemBase();
    QString typeName(int type = -5);
    int getItemId(){return this->item_id;}
    void notDrag(bool val = false);
    void setAllowCopy(bool which){allowCopy = which;}
    void setTransformControls();

    void setRect(QRectF rect);
    QRectF getRect()const{return this->_rect;}
    void setTransformable(bool trans);
    bool isTransformable(){return transformable;}
    void setMovable(bool which);
    QString itemId(){return m_id;}


    //layout
    virtual void setGeometry(const QRectF &rect);
    virtual QSizeF sizeHint(Qt::SizeHint which, const QSizeF &constraint) const;
protected:
    virtual void init();
    virtual QRectF boundingRect()const;
    virtual QPainterPath shape()const ;
    virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value);
    virtual void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
    virtual  void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    virtual  void mousePressEvent(QGraphicsSceneMouseEvent *event);
    virtual  void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);

    QRectF _rect;
    virtual void updateItemContents();
    void showCorners(bool which);
    int item_id;
    bool allowCopy;
   // qreal _x,_y,_w,_h;

public slots:
    void setItemSelected();
    void setPosX(double);
    void setPosY(double);
    void setPosZ(double);
    void setW(double);
    void setH(double);
    void setItemId(QString _id){m_id = _id;}

signals:
    void selectedChanged(ItemBase* item);//this would be used to notify toolbars
    void positionChanged(qreal x = 0,qreal y = 0,qreal z = 0);
    void testSignal();

private:
    QString m_id;
    SizeGripItem* m_transform_controlls;
    bool transformable;
    bool initialized ;
    void initEvents();
    QColor _normal,_selected,_border;
};


/**
    Resizer control needed by SizeGripItem, to Resize Items.
*/
class ItemBaseResizer : public SizeGripItem::Resizer
{
    public:
        virtual void operator()(QGraphicsItem* item, const QRectF& rect)
        {
            ItemBase* b_item =
                dynamic_cast<ItemBase*>(item);

            if (b_item)
            {
                b_item->setRect(rect);
            }
        }
};

#endif // ITEMBASE_H
