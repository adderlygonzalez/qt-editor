#ifndef DOCUMENTPANEL_H
#define DOCUMENTPANEL_H
#include"panelitem.h"

/**
  This element will be the base in the scene, containing all of the layers,panels and custom items.
*/
class DocumentPanel: public PanelItem
{
public:
  explicit DocumentPanel(QGraphicsItem* parent = NULL);
  DocumentPanel(QRect rect,QGraphicsItem* parent = NULL);
  virtual void init();
protected:
  virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);


private:

};

#endif // DOCUMENTPANEL_H
