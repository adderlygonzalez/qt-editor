#ifndef CHECKBOXITEM_H
#define CHECKBOXITEM_H
#include"itembase.h"

class CheckBoxItem:public ItemBase
{
public:  
  enum{Type=CheckBoxType};
  int type() const { return Type;}
  CheckBoxItem(QRectF rect = QRectF(0,0,20,20),QGraphicsItem* parent = NULL);
protected:
  void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
};

#endif // CHECKBOXITEM_H
