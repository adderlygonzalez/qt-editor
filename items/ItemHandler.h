#ifndef ITEMHANDLER_H
#define ITEMHANDLER_H

#include"coloritem.h"
#include"textitem.h"
#include"buttonitem.h"
#include"radiobuttonitem.h"
#include"slideritem.h"
#include"checkboxitem.h"
#include"panelitem.h"

template<typename Type> ItemBase* createType() {
    return new Type;
}
class ItemHandler
{
public:
    /***/
    typedef ItemBase* (*ComponentFactoryFuncPtr)();

    template<typename Type>
    ComponentFactoryFuncPtr create(int type)
    {
        ComponentFactoryFuncPtr item;
        if(type ==  ColorItem::Type){
           item = createType<ColorItem>;
        }
        return item;
    }
    /***/


    template<class T> T* createNew(int type)
    {
        T* item = NULL;
        switch(type)
        {
            case ColorItem::Type:{
                item = new ColorItem;
            }
                break;
            case TextItem::Type:{
                item = new TextItem;
            }
                break;

            case PanelItem::Type:{
                item = new PanelItem;
            }
                break;

          case ButtonItem::Type:{
              item = new ButtonItem;
          }
              break;
          case RadioButtonItem::Type:{
              item = new RadioButtonItem;
          }
              break;
          case CheckBoxItem::Type:{
              item = new CheckBoxItem;
          }
              break;
          case SliderItem::Type:{
              item = new SliderItem;
          }
              break;
            default:
                item = new ItemBase;
                break;
        }
         return item;
    }

    template<class T,class I>
    T* createItem(I const& type)
    {
        T* item ;
        if(type == itemTypes::COLOR_ITEM){
            item =  new T();
        }else{
            item = NULL;
        }
        return item;
    }


};

#endif // ITEMHANDLER_H
