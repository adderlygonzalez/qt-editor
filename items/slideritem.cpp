#include "slideritem.h"

SliderItem::SliderGrabItem::SliderGrabItem(QRectF rect ,QGraphicsItem *parent):QGraphicsRectItem(rect,parent)
{
  setFlag(QGraphicsItem::ItemIsMovable);
}
void SliderItem::SliderGrabItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
  QGraphicsRectItem::mousePressEvent(event);
}
void SliderItem::SliderGrabItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
  QGraphicsRectItem::mouseMoveEvent(event);
}


SliderItem::SliderItem(QRectF rect , QGraphicsItem* parent):ItemBase(parent,rect)
{
  m_grab = new SliderGrabItem(QRectF(0,0,5,rect.height()),this);
  m_grab->setPos(this->pos().x() + this->_rect.width()/2,this->pos().y());
}
void SliderItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  QPen pen(QColor(Qt::black));
  painter->setPen(pen);

  painter->drawRect( _rect);
  if(isSelected())
    {
      painter->setBrush(QBrush(QColor(Qt::gray)));
    }else{
      painter->setBrush(QBrush(QColor(Qt::lightGray)));
    }
    painter->drawRect(QRectF(3,3,_rect.width() - 5,_rect.height() - 5));
}


