
#include"ItemHandler.h"
#ifndef PANELITEM_H
#define PANELITEM_H


/**
    This item will contain other items. Kind of a container.
*/
class PanelItem: public ItemBase
{
public:
    enum{Type= PanelType};
     virtual int type() const { return Type;}
     explicit PanelItem(QGraphicsItem* parent = NULL);
     explicit PanelItem(QRect rect,QGraphicsItem* parent = NULL);

    void setSeletedColor(QColor c){_selectedDrawColor = c;}
    QColor getSelectedColor(){return _selectedDrawColor;}
    void setDragColor(QColor c){_dragColor = c;}
    QColor getDragColor(){return _dragColor;}
    void setDrawColor(QColor c){_drawColor = c;}
    QColor getDrawColor(){return _drawColor;}
protected:
    virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value);
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    virtual void dragEnterEvent(QGraphicsSceneDragDropEvent *event);
    virtual void dragMoveEvent(QGraphicsSceneDragDropEvent *event);
    virtual void dropEvent(QGraphicsSceneDragDropEvent *event);
    virtual void init();
    virtual void initLayout();
private:
    QGraphicsLinearLayout *layout;
    bool _isDragging;
    QColor _drawColor;
    QColor _dragColor;
    QColor _selectedDrawColor;
};

#endif // PANELITEM_H
