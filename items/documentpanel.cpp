#include "documentpanel.h"

DocumentPanel::DocumentPanel(QGraphicsItem* parent):PanelItem(parent)
{
  init();
}

DocumentPanel::DocumentPanel(QRect rect, QGraphicsItem *parent):PanelItem(rect,parent)
{
  init();
}

void DocumentPanel::init()
{
  setMovable(false);
  setFlag(QGraphicsItem::ItemIsFocusable, false);
  setFlag(QGraphicsItem::ItemIsSelectable, false);
}

void DocumentPanel::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  Q_UNUSED(option)
  Q_UNUSED(widget)
  QLineF top = QLineF(_rect.topLeft(),_rect.topRight());
  QLineF right =  QLineF(_rect.topRight(),_rect.bottomRight());
  QLineF bottom =  QLineF(_rect.bottomRight(),_rect.bottomLeft());
  QLineF left =  QLineF(_rect.bottomLeft(),_rect.topLeft());

  painter->setBrush(QBrush(Qt::white));
  painter->drawRect(_rect);
  /*
  if(isSelected()){
      painter->setPen(_selectedDrawColor);
  }else{
      painter->setPen(_drawColor);
  }*/
  QVector<QLineF> lines ;
  lines.append(top);
  lines.append(right);
  lines.append(bottom);
  lines.append(left);
  painter->setPen(QPen(Qt::SolidLine));
  painter->drawLines(lines);
}

