#ifndef LAYERPANEL_H
#define LAYERPANEL_H
#include"panelitem.h"

class LayerPanel:public PanelItem
{
public:
  enum{Type= LayerType};
   virtual int type() const { return Type;}
  LayerPanel(QGraphicsItem* parent = NULL);
  LayerPanel(QRect rect,QGraphicsItem* parent = NULL);
  virtual void init();
};

#endif // LAYERPANEL_H
