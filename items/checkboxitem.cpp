#include "checkboxitem.h"

CheckBoxItem::CheckBoxItem(QRectF rect,QGraphicsItem* parent):ItemBase(parent,rect)
{
}

void CheckBoxItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
  QPen pen(QColor(Qt::black));
  painter->setPen(pen);

  painter->drawRect( _rect);
  if(isSelected())
    {
      painter->setBrush(QBrush(QColor(Qt::gray)));
    }else{
      painter->setBrush(QBrush(QColor(Qt::lightGray)));
    }
    painter->drawRect(QRectF(3,3,_rect.width() - 5,_rect.height() - 5));
}
