#include "buttonitem.h"

ButtonItem::ButtonItem(QString text,QRectF rect ,QGraphicsItem* parent):TextItem(text,rect,parent)
{
    centerText();
}

void ButtonItem::init()
{
  TextItem::init();
}

void ButtonItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)
    painter->setBrush(QBrush(Qt::gray));
    painter->drawRoundedRect(boundingRect(),20.0,15.0);
}


void ButtonItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    if (event->button() != Qt::LeftButton) {
        event->ignore();
        return;
      }
      setCursor(Qt::ClosedHandCursor);
      TextItem::mousePressEvent(event);
}

void ButtonItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    if (QLineF(event->screenPos(), event->buttonDownScreenPos(Qt::LeftButton))
        .length() < QApplication::startDragDistance()) {
        return;
      }
    if(allowCopy){
        QDrag *drag = new QDrag(event->widget());
        QMimeData *mime = new QMimeData;
        drag->setMimeData(mime);

        static int n = 0;
        if (n++ > 2 && (qrand() % 3) == 0) {
            QImage image(":/images/head.png");
            mime->setImageData(image);

            drag->setPixmap(QPixmap::fromImage(image).scaled(30, 40));
            drag->setHotSpot(QPoint(15, 30));
          } else {
            mime->setProperty("item_id",Type);

            QPixmap pixmap(34, 34);
            pixmap.fill(Qt::white);

            QPainter painter(&pixmap);
            painter.translate(15, 15);
            painter.setRenderHint(QPainter::Antialiasing);
            paint(&painter, 0, 0);
            painter.end();

            pixmap.setMask(pixmap.createHeuristicMask());

            drag->setPixmap(pixmap);
            drag->setHotSpot(QPoint(15, 20));
          }

        drag->exec();
        setCursor(Qt::OpenHandCursor);
    }else{
        QGraphicsWidget::mouseMoveEvent(event);
    }
}
