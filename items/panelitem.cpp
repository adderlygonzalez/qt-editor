#include "panelitem.h"

PanelItem::PanelItem(QGraphicsItem* parent): ItemBase(parent,QRect(0,0,150,150))
{

    init();

}
PanelItem::PanelItem(QRect rect,QGraphicsItem *parent): ItemBase(parent,rect)
{
    init();
}

void PanelItem::init()
{
  layout  = new QGraphicsLinearLayout(Qt::Horizontal);
  //layout->setPreferredSize(QSize(300,300));
  //layout->setSpacing(1);
  this->setLayout(layout);
    //setFlag(ItemIsPanel);
     _drawColor = QColor(Qt::black);
     _selectedDrawColor = QColor(Qt::red);
    this->setAcceptDrops(true);
    setObjectName(MAIN_PANEL);
    updateItemContents();
}

void PanelItem::initLayout(){}


QVariant PanelItem::itemChange(GraphicsItemChange change, const QVariant &value)
{
  if(change == ItemSendsGeometryChanges){
      setGeometry(boundingRect());
    }
  return ItemBase::itemChange(change,value);
}

void PanelItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option)
    Q_UNUSED(widget)
    if(isSelected()){
        painter->setPen(_selectedDrawColor);
    }else{
        painter->setPen(_drawColor);
    }
    QLineF top = QLineF(_rect.topLeft(),_rect.topRight());
    QLineF right =  QLineF(_rect.topRight(),_rect.bottomRight());
    QLineF bottom =  QLineF(_rect.bottomRight(),_rect.bottomLeft());
    QLineF left =  QLineF(_rect.bottomLeft(),_rect.topLeft());

    QVector<QLineF> lines ;
    lines.append(top);
    lines.append(right);
    lines.append(bottom);
    lines.append(left);
    painter->setPen(QPen(Qt::DashDotLine));
    painter->drawLines(lines);
}

void PanelItem::dragEnterEvent(QGraphicsSceneDragDropEvent *event)
{
    qDebug() << " drag item enter" ;
    event->accept();
    QGraphicsItem::dragEnterEvent(event);
}

void PanelItem::dragMoveEvent(QGraphicsSceneDragDropEvent *event)
{
    qDebug() << " drag item move" ;
    event->accept();
    QGraphicsItem::dragMoveEvent(event);
}

void PanelItem::dropEvent(QGraphicsSceneDragDropEvent *event)
{
    const QMimeData* mime = event->mimeData();
     qDebug() << " drop item type =" << mime->property("item_id");
     if(!mime->property("item_id").isNull()){
         event->setAccepted(true);
         ItemHandler* handler = new ItemHandler;

         ItemBase* item = handler->createNew<ItemBase>(mime->property("item_id").toInt());
         if(item){
             item->setRect(QRect(0,0,DEFAULT_ITEM_SIZE::WIDTH,DEFAULT_ITEM_SIZE::HEIGHT));
             item->setPos(mapFromParent(event->scenePos()));
             item->notDrag();
             item->setTransformable(true);
             if(layout){
                 layout->addItem(item);
                 layout->setStretchFactor(item,1);
                 //this->setFlag(ItemChildAddedChange, true);
                 //layout->addItem(item);
               }else item->setParentItem(this);
             qDebug() << "ms added item ";
         }
     }else{
         QGraphicsItem::dropEvent(event);
     }
}
