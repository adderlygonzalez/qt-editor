#ifndef TEXTITEM_H
#define TEXTITEM_H
#include"textwidget.h"
#include"itembase.h"


/**
    Item for representing text, is somewhat editable throught a proxywidget.

*/
class TextItem: public ItemBase
{
public:
    enum{AlignTop=1,AlignBottom,AlignRight,AlignLeft,AlignCenter};
    enum{Type = TextType};
    virtual int type()const {return Type;}
    TextItem(QString text = QString(),QRectF rect = QRectF(),QGraphicsItem* parent = NULL);
    void init();
    void initTextWidget();
    QString getText();
    qreal  getFontSize();
    void setFontSize(qreal size);
    int getTextAlignment() const {return ALIGNED;}
public slots:
    void setText(const QString text);
    void hideTextWidget();    
    void centerText(int align = AlignCenter);
    void center(QVariant v);
    void positionText(int);
signals:
    void textChanged(QString str = QString());
    void getWidgetText();
protected:
    virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    virtual void mousePressEvent(QGraphicsSceneMouseEvent *event);
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    virtual void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event);
    QGraphicsSimpleTextItem* text ;
private:
    int ALIGNED;//contains the alignment of the text
    bool m_text_initialized;
    QGraphicsProxyWidget* m_proxy;
    TextWidget* text_widget;
    QString m_text;
};

#endif // TEXTITEM_H
