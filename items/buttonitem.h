#ifndef BUTTONITEM_H
#define BUTTONITEM_H
#include"textitem.h"

class ButtonItem : public TextItem
{
public:
    enum{Type= ButtonType};
    int type() const { return Type;}
    explicit ButtonItem(QString text = QString(),QRectF rect = QRectF(),QGraphicsItem* parent = NULL);
    void init();

public slots:

protected:
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    
};

#endif // BUTTONITEM_H
