#include "itembase.h"
#include"mainwidget.h"


ItemBase::ItemBase(QGraphicsItem* parent,QRectF rect):QGraphicsWidget(parent),
    _rect(rect),initialized(false),m_transform_controlls(NULL)
{
    init();
}
ItemBase::~ItemBase(){

}
QString ItemBase::typeName(int type){
    if(type == -5) type = this->type();
  switch(type)
    {
    case ItemBase::DocumentPanelType:{ return QString("DocumentPanel");} break;
    case ItemBase::LayerType:{ return QString("LayerPanel");} break;
    case ItemBase::PanelType:{ return QString("PanelItem");} break;
    case ItemBase::TextType:{ return QString("TextItem");} break;
    case ItemBase::ButtonType:{ return QString("ButtonItem");} break;
    case ItemBase::ColorType:{ return QString("ColorItem");} break;
     default:
      break;
    }
}

QRectF ItemBase::boundingRect() const
{
  return _rect;// QRectF(QPointF(0,0),geometry().size());
}
QPainterPath ItemBase::shape() const
{
    QPainterPath path;
    path.addRect(boundingRect());
    return path;
}

void ItemBase::setGeometry(const QRectF &rect)
{
  prepareGeometryChange();
  QGraphicsWidget::setGeometry(rect);
 // setPos(rect.topLeft());
}

QSizeF ItemBase::sizeHint(Qt::SizeHint which, const QSizeF &constraint) const
{
  switch (which) {
     case Qt::MinimumSize:
     case Qt::PreferredSize:
         // Do not allow a size smaller than the pixmap with two frames around it.
         return  QSize(12, 12);
     case Qt::MaximumSize:
         return QSizeF(1000,1000);
     default:
         break;
     }
    return constraint;
}

void ItemBase::init()
{
    this->setMovable(true);
    setFlag(ItemIsSelectable);
    setAcceptHoverEvents(true);
    setTransformable(false);
}

void ItemBase::initEvents()
{
    if(!initialized){
        //MainWidget* mainW = MainWidget::instance();
        //ToolWidget* tool  = mainW->getToolWidget();
        //connect(this,&ItemBase::selectedChanged,tool,&ToolWidget::selectedItemChanged);
    }
}
void ItemBase::setRect(QRectF rect){
    this->_rect = rect;
    setGeometry(rect);
    this->setFlag(ItemSendsGeometryChanges, true);
}
void ItemBase::setItemSelected()
{
    emit selectedChanged(this);
}
void ItemBase::setPosX(double x){
    setPos((qreal)x,pos().y());
    setFlag(ItemSendsScenePositionChanges, true);
}
void ItemBase::setPosY(double y){
    setPos(pos().x(),(qreal)y);
    setFlag(ItemSendsScenePositionChanges, true);
}
void ItemBase::setPosZ(double z){
    setZValue((qreal)z);
    this->setFlag(ItemSendsGeometryChanges, true);
}

void ItemBase::setW(double width){
    this->setFlag(ItemSendsGeometryChanges, false);
    if(!_rect.isNull()){
        _rect.setWidth((qreal)width);
    }
  this->setFlag(ItemSendsGeometryChanges, true);
}
void ItemBase::setH(double height){
    this->setFlag(ItemSendsGeometryChanges, false);
    if(!_rect.isNull()){
        _rect.setHeight((qreal)height);
    }
    this->setFlag(ItemSendsGeometryChanges, true);
}
void ItemBase::updateItemContents()
{
    update(boundingRect());
}
void ItemBase::showCorners(bool which)
{
    if(m_transform_controlls != NULL)
    {
        if(which){
            m_transform_controlls->showHandles();
        }else{
            if(!isSelected())
                m_transform_controlls->hideHandles();
        }
    }

}

void ItemBase::notDrag(bool val)
{
    allowCopy = val;
}

void ItemBase::setTransformControls(){
    m_transform_controlls = new SizeGripItem(new ItemBaseResizer,this);
}
void ItemBase::setTransformable(bool trans)
{
    transformable = trans;
    if(trans)
        setTransformControls();
    else if(m_transform_controlls){
        //TODO: transform control
    }
}
void ItemBase::setMovable(bool which)
{
    if(which){
        setFlag(QGraphicsItem::ItemIsMovable);
    }else{
        setFlag(QGraphicsItem::ItemIsMovable,false);
    }
}

QVariant ItemBase::itemChange(GraphicsItemChange change, const QVariant &value)
{
    if(change == ItemSelectedChange){
        initEvents();
        setItemSelected();
    }else if(change == ItemPositionChange){
        emit positionChanged(pos().x(),pos().y(),zValue());
    }else if(change == ItemSendsGeometryChanges){
        setGeometry(_rect);
    }
    return QGraphicsWidget::itemChange(change,value);
}

void ItemBase::hoverEnterEvent(QGraphicsSceneHoverEvent *event){
    Q_UNUSED(event)
   // showCorners(true);
}

void ItemBase::hoverLeaveEvent(QGraphicsSceneHoverEvent *event){
    Q_UNUSED(event);
   // showCorners(false);
}

void ItemBase::mousePressEvent(QGraphicsSceneMouseEvent *event){
    QGraphicsWidget::mousePressEvent(event);
}

void ItemBase::mouseMoveEvent(QGraphicsSceneMouseEvent *event){
    QGraphicsWidget::mouseMoveEvent(event);
}

void ItemBase::mouseReleaseEvent(QGraphicsSceneMouseEvent *event){
    QGraphicsWidget::mouseReleaseEvent(event);
}

