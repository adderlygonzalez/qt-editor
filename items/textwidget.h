#ifndef TEXTWIDGET_H
#define TEXTWIDGET_H
#include<QtWidgets>

class TextWidget: public QTextEdit
{
public:
    TextWidget(QWidget* parent = NULL);
    void init();
protected:
    virtual void keyReleaseEvent(QKeyEvent *event);
};

#endif // TEXTWIDGET_H
