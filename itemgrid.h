#ifndef ITEMGRID_H
#define ITEMGRID_H
#include <QtWidgets>
#include <QtUiTools>
#include"items/coloritem.h"
#include"items/buttonitem.h"
#include"items/textitem.h"
#include"items/SizeGripItem.h"

class ItemGrid: public QGraphicsView
{
    Q_OBJECT
public:
    ItemGrid(QWidget* parent = 0);
    void init();
    void loadItems();
    void addItemLayout(QGraphicsLayoutItem* item,int row = 0,int column = 0);
private:
    void setUpView();
    QGraphicsScene *scene;
    QGraphicsWidget* item_grid;
};

#endif // ITEMGRID_H
