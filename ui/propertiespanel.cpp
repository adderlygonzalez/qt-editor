#include "propertiespanel.h"
#include "ui_propertiespanel.h"

PropertiesPanel::PropertiesPanel(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::PropertiesPanel)
{
    ui->setupUi(this);
    this->setDisabled(true);
    init();
}

void PropertiesPanel::init()
{
    //fields in the ui file
    m_x = this->findChild<QDoubleSpinBox*>("x");
    m_y = this->findChild<QDoubleSpinBox*>("y");
    m_z = this->findChild<QDoubleSpinBox*>("z");
    m_w = this->findChild<QDoubleSpinBox*>("w");
    m_h = this->findChild<QDoubleSpinBox*>("h");
    m_id = this->findChild<QLineEdit*>("id");
    //ui->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Expanding);
}
void PropertiesPanel::setSelectedItem(ItemBase* tmp_item){
    if(m_selected_item)
        disconnectItemFromFields();
    this->m_selected_item = tmp_item;
    connectItemToFields();
    setSelectedItemValues();
}

void PropertiesPanel::connectItemToFields()
{
    if(m_selected_item){
       connections.append( connect(m_x,SIGNAL(valueChanged(double)),m_selected_item,SLOT(setPosX(double))));
       connections.append( connect(m_y,SIGNAL(valueChanged(double)),m_selected_item,SLOT(setPosY(double))));
       connections.append( connect(m_z,SIGNAL(valueChanged(double)),m_selected_item,SLOT(setPosZ(double))));
       connections.append( connect(m_w,SIGNAL(valueChanged(double)),m_selected_item,SLOT(setW(double))));
       connections.append( connect(m_h,SIGNAL(valueChanged(double)),m_selected_item,SLOT(setH(double))));
       connections.append( connect(m_id,SIGNAL(textChanged(QString)),m_selected_item,SLOT(setItemId(QString))));
       connections.append( connect(m_selected_item,SIGNAL(positionChanged()),this,SLOT(setSelectedItemValues())));
    }
}
void PropertiesPanel::disconnectItemFromFields()
{
    QList<QMetaObject::Connection>::Iterator it ;
    for(it = connections.begin();it != connections.end();it++){
        disconnect(*it);
    }
}
void PropertiesPanel::setSelectedItemValues(){
    if(m_selected_item){
        QPointF i_pos = m_selected_item->pos();
        QRectF i_rect = m_selected_item->getRect();
        m_x->setValue(i_pos.x());
        m_y->setValue(i_pos.y());
        m_z->setValue(m_selected_item->zValue());
        m_w->setValue(i_rect.width());
        m_h->setValue(i_rect.height());
        m_id->setText(m_selected_item->itemId());
    }
}

PropertiesPanel::~PropertiesPanel()
{
    delete ui;
}
