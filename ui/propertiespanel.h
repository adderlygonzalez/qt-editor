#ifndef PROPERTIESPANEL_H
#define PROPERTIESPANEL_H

#include <QtWidgets>
#include"items/itembase.h"

namespace Ui {
class PropertiesPanel;
}

class PropertiesPanel : public QWidget
{
    Q_OBJECT
    Q_PROPERTY(QDoubleSpinBox* x READ getX WRITE setX)
    Q_PROPERTY(QDoubleSpinBox* y READ getY WRITE setY)
    Q_PROPERTY(QDoubleSpinBox* z READ getZ WRITE setZ)
    Q_PROPERTY(QDoubleSpinBox* w READ getW WRITE setW)
    Q_PROPERTY(QDoubleSpinBox* h READ getH WRITE setH)
    Q_PROPERTY(QLineEdit* id READ getId WRITE setId)

    Q_PROPERTY(ItemBase* selected_item READ getSelectedItem WRITE setSelectedItem)
public:
    explicit PropertiesPanel(QWidget *parent = 0);
    void init();
    void connectItemToFields();
    void disconnectItemFromFields();
    ~PropertiesPanel();

    void setX(QDoubleSpinBox* x){ m_x = x;}
    void setY(QDoubleSpinBox* y){ m_y = y;}
    void setZ(QDoubleSpinBox* z){ m_z = z;}
    void setW(QDoubleSpinBox* w){ m_w = w;}
    void setH(QDoubleSpinBox* h){ m_h = h;}
    void setId(QLineEdit* id){m_id = id;}

    QDoubleSpinBox* getX(){ return m_x;}
    QDoubleSpinBox* getY(){ return m_y;}
    QDoubleSpinBox* getZ(){ return m_z;}
    QDoubleSpinBox* getW(){ return m_w;}
    QDoubleSpinBox* getH(){ return m_h;}
    QLineEdit* getId(){return m_id;}

    ItemBase* getSelectedItem(){return m_selected_item;}

public slots:
    void setSelectedItem(ItemBase* tmp_item);
    void setSelectedItemValues();

private:
    QList<QMetaObject::Connection> connections;
    ItemBase* m_selected_item;
    QDoubleSpinBox* m_x ;
    QDoubleSpinBox* m_y ;
    QDoubleSpinBox* m_z;//index in the scene.
    QDoubleSpinBox* m_w ;
    QDoubleSpinBox* m_h ;
    QLineEdit* m_id;
    Ui::PropertiesPanel *ui;
};

#endif // PROPERTIESPANEL_H
