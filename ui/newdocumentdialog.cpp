#include "newdocumentdialog.h"
#include "ui_newdocumentdialog.h"

NewDocumentDialog::NewDocumentDialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::NewDocumentDialog)
{
  ui->setupUi(this);
  ok = false;
  initElements();
}

NewDocumentDialog::~NewDocumentDialog()
{
  delete ui;
}

void NewDocumentDialog::initElements()
{
  char* resolutions[] = {"800x600","1024*766","1366*768"};
  QStringList res;
  for(int x =0;x < sizeof(resolutions)/sizeof(resolutions[0]);x++)
    res.append(resolutions[x]);

  for(int n = 0;n < sizeof(DOC_TYPES)/sizeof(DOC_TYPES[0]);n++)
    {
      switch(DOC_TYPES[n])
        {
        case 1:
          {
           docAndOptions.insert(QString(DOC_TYPES_NAMES[n]),res);
          }break;
        case 2:
          {
            docAndOptions.insert(QString(DOC_TYPES_NAMES[n]),res);
          }break;
        case 3:
          {
            docAndOptions.insert(QString(DOC_TYPES_NAMES[n]),res);
          }break;
        }

      ui->docType->addItem(new QListWidgetItem(QString(DOC_TYPES_NAMES[n])));
    }
}
void NewDocumentDialog::setOptionsOfDoc(QString docName)
{
     QStringList list = docAndOptions.value(docName);
      ui->docOptions->clear();
     if(!list.isEmpty())
       {
         for(int n = 0 ; n < list.size();n++)
           {
             ui->docOptions->addItem(new QListWidgetItem(list.at(n)));
           }
       }
}


void NewDocumentDialog::on_docType_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
  selectedDoc = current->text();
  setOptionsOfDoc(current->text());
  ui->accept->setEnabled(false);
}

void NewDocumentDialog::on_docOptions_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous)
{
  if(current)
    {
      selectedOption = current->text();
      ui->accept->setEnabled(true);
    }else if(previous){
      selectedOption = previous->text();
      ui->accept->setEnabled(true);
    }
}

void NewDocumentDialog::on_accept_clicked()
{
  qDebug() << "doc" << selectedDoc;
  qDebug() << "options" << selectedOption;
  if(!ui->name->text().isEmpty())
    {
        docName = ui->name->text();
        ok = true;
        this->hide();
        emit createNewAccepted(this);
    }
}

void NewDocumentDialog::on_cancel_clicked()
{
  this->close();
}
