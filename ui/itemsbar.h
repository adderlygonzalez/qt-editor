#ifndef ITEMSBAR_H
#define ITEMSBAR_H

#include <QWidget>
#include <QPixmap>
#include <QLabel>
#include <QHBoxLayout>
#include <QMouseEvent>
#include <QDrag>
#include <QMimeData>
#include<QSpacerItem>
#include"items/itembase.h"

namespace Ui {
  class ItemsBar;
}

class BarItem: public QWidget
{
public:
  BarItem(QWidget* parent = NULL);
  BarItem(QString imagefile,QSize size = QSize(),int type = 0);
  void construct(QString imagefile,QSize size = QSize(),int type = 0);
  void resizePixmap(QSize size);
  void init();
protected:
  void paintEvent(QPaintEvent *);
  void mousePressEvent(QMouseEvent *);
  void mouseMoveEvent(QMouseEvent *);
  void mouseReleaseEvent(QMouseEvent *);
private:
  QLabel* lbl;
  QString tipMessage;
  QPixmap pixmap;
  int type;
};

class ItemsBar : public QWidget
{
  Q_OBJECT

public:
  explicit ItemsBar(QWidget *parent = 0);
  ~ItemsBar();
  void prepare();
  void addItem(QWidget* item);
private:
  Ui::ItemsBar *ui;
};

#endif // ITEMSBAR_H
