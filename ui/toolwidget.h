#ifndef TOOLWIDGET_H
#define TOOLWIDGET_H

#include <QWidget>
#include"ui/propertiespanel.h"
#include"items/itembase.h"
#include"mainscene.h"

namespace Ui {
class ToolWidget;
}


class ToolWidget : public QWidget
{
    Q_OBJECT
public:
    explicit ToolWidget(QWidget *parent = 0);
     ~ToolWidget();
    void setMainScene(MainScene* scene);
signals:
    
public slots:
    void sceneSelectionChanged();
    
private:
    void connectToolBars();
    void readItems();
    void resetItems();
    void prepareControls();

    PropertiesPanel* p_panel;
    MainScene* mainScene;
    Ui::ToolWidget *ui;
};

#endif // TOOLWIDGET_H
