#include "itemsbar.h"
#include "ui_itemsbar.h"

BarItem::BarItem(QWidget *parent):QWidget(parent)
{
  init();
}

BarItem::BarItem(QString imagefile, QSize size,int type)
{
  setMouseTracking(true);
  setAutoFillBackground(true);
  construct(imagefile,size,type);
}
void BarItem::resizePixmap(QSize size)
{
  if(!size.isNull())
    {
      if(lbl)
        {
          const QPixmap* pix = lbl->pixmap();
          lbl->setPixmap(pix->scaled(size.width(),size.height(),Qt::KeepAspectRatio));
        }
    }
}
void BarItem::construct(QString imagefile, QSize size,int type )
{
  QHBoxLayout* layout = new QHBoxLayout;
  int w = size.width();
  int h = size.height();
  this->type = type;
  pixmap =QPixmap(imagefile);
  if(pixmap.isNull() ){
      pixmap = QPixmap(size.width(),size.height());
      pixmap.fill(QColor(Qt::gray));
    }else{
      w = pixmap.width();
      h = pixmap.height();
    }
  lbl = new QLabel(this);
  lbl->setPixmap(pixmap);
 // lbl->setPixmap(pixmap.scaled(w,h,Qt::KeepAspectRatio));
   lbl->resize(size.width(),size.height());

  layout->addWidget(lbl,0,Qt::AlignCenter);
  this->setLayout(layout);
}

void BarItem::init()
{
  construct("",QSize(20,20));
}
void BarItem::paintEvent(QPaintEvent *e)
{
}

void BarItem::mousePressEvent(QMouseEvent *event)
{
  if (event->buttons() == Qt::LeftButton) {
      lbl->setPixmap(pixmap.scaled(pixmap.width()+5,pixmap.height()+5));
    }else{
      lbl->setPixmap(pixmap);
    }
    setCursor(Qt::ClosedHandCursor);
  QWidget::mousePressEvent(event);
}
void BarItem::mouseMoveEvent(QMouseEvent *event)
{
  if (event->buttons() == Qt::LeftButton) {
      QDrag *drag = new QDrag(parentWidget());
      QMimeData *mime = new QMimeData;
      mime->setProperty("item_id",type);
      drag->setMimeData(mime);

      drag->exec();
      setCursor(Qt::OpenHandCursor);
    }else  if (this->rect().contains(event->pos())) {
      // Mouse over Widget
      qDebug()  << "over";
  }

  QWidget::mouseMoveEvent(event);
}

void BarItem::mouseReleaseEvent(QMouseEvent *event)
{
  if (event->button() == Qt::LeftButton) {
      lbl->setPixmap(pixmap);
    }
  QWidget::mouseReleaseEvent(event);
}

ItemsBar::ItemsBar(QWidget *parent) :
  QWidget(parent),
  ui(new Ui::ItemsBar)
{
  ui->setupUi(this);
  prepare();
}
void ItemsBar::prepare()
{
  int items[] = {
                  ItemBase::PanelType,ItemBase::ColorType,
                  ItemBase::ButtonType,ItemBase::TextType,
                  ItemBase::LayerType,ItemBase::RadioButtonType,
                  ItemBase::CheckBoxType,ItemBase::SliderType
                };
  QString images[] =
  {
    ":/images/icons/panel.png",":/images/icons/circle.png",":/images/icons/btn.png",
    ":/images/icons/txt.png",":/images/icons/layer.png",":/images/icons/circle.png",
    ":/images/icons/checked.png",":/images/icons/onswitch.png"
  };
  int colWidth = 0;
  for(unsigned int n = 0;n < sizeof(items)/sizeof(items[0]);n++)
  {
      BarItem* item = new BarItem(images[n],QSize(20,20),items[n]);
      if(item->width() > colWidth){
          colWidth = item->width();
        }
      item->resizePixmap(QSize(20,20));
      ui->baseLayout->addWidget(item,n,0);
  }  
  //this->setMaximumWidth(colWidth);
  //ui->baseLayout->setSizeConstraint(QLayout::SetMaximumSize);
}

void ItemsBar::addItem(QWidget *item)
{
  ui->baseLayout->addWidget(item);
}

ItemsBar::~ItemsBar()
{
  delete ui;
}
