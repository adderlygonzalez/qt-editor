#ifndef NEWDOCUMENTDIALOG_H
#define NEWDOCUMENTDIALOG_H

#include <QDialog>
#include<QDebug>
#include <QListWidgetItem>
#include <QMap>

namespace Ui {
  class NewDocumentDialog;
}
static const int DOC_TYPES[] = {1,2,3};
static const char* DOC_TYPES_NAMES[] = {"JSON","XML","GPFORM"};


class NewDocumentDialog : public QDialog
{
  Q_OBJECT
  
public:

  explicit NewDocumentDialog(QWidget *parent = 0);
  ~NewDocumentDialog();
  void initElements();
  void setOptionsOfDoc(QString docName);

  QString selectedDoc;
  QString selectedOption;
  QString docName;
  bool ok;
  
private slots:

  void on_docType_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

  void on_docOptions_currentItemChanged(QListWidgetItem *current, QListWidgetItem *previous);

  void on_accept_clicked();

  void on_cancel_clicked();
signals:
  void createNewAccepted(NewDocumentDialog*);
private:
  //this will store the doc and its options
  QMap<QString,QStringList> docAndOptions;
  //QMap<QString,QMap<int,QString> > docAndOptions;
  Ui::NewDocumentDialog *ui;
};

#endif // NEWDOCUMENTDIALOG_H
