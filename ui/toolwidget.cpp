#include "toolwidget.h"
#include "ui_toolwidget.h"

ToolWidget::ToolWidget(QWidget *parent) :
    QWidget(parent),ui(new Ui::ToolWidget)
{
    ui->setupUi(this);
    p_panel = new PropertiesPanel;
   QVBoxLayout* tool_layout = this->findChild<QVBoxLayout*>("v_layout");
   tool_layout->addWidget(p_panel);
   //this->setLayout(tool_layout);
   prepareControls();
}

void ToolWidget::setMainScene(MainScene *scene)
{
  mainScene = scene;
  connectToolBars();
}

void ToolWidget::connectToolBars()
{
  if(mainScene)
    {
      connect(mainScene,&MainScene::selectionChanged,this,&ToolWidget::sceneSelectionChanged);
    }
}

void ToolWidget::readItems()
{
    QList<QGraphicsItem*> s_items = mainScene->selectedItems();
    if(!s_items.isEmpty() && s_items.size() > 0)
    {
        p_panel->setDisabled(false);
        p_panel->setSelectedItem(dynamic_cast<ItemBase*>(s_items.at(0)));
    }else{
        p_panel->setDisabled(true);
    }
}
void ToolWidget::prepareControls()
{
}
void ToolWidget::sceneSelectionChanged()
{
  readItems();
}

ToolWidget::~ToolWidget()
{
    delete ui;
}
