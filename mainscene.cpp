#include "mainscene.h"

MainScene::MainScene(QObject* parent):QGraphicsScene(parent)
{
    init();
}

void MainScene::init()
{
    /*ruler = new SceneRuler(QRectF(0,0,20,1000));
    ruler->setPos(-20,-20);
    this->addItem(ruler);*/
    m_selecting = false;
    m_select_rect.setRect(0,0,10,10);
    m_select_rect.setZValue(100);
    m_select_rect.setBrush(QBrush(Qt::red));
    m_select_rect.setVisible(false);
    this->addItem(&m_select_rect);
    setStickyFocus(false);
    setBackgroundBrush(QBrush(Qt::gray));
    setItemIndexMethod(NoIndex);
}

void MainScene::startSelecting()
{
    m_selecting = true;
    if(m_selecting){
        m_select_rect.setVisible(true);
        m_select_rect.setPos(m_start_selecting);
        m_select_rect.setFlag(QGraphicsItem::ItemSendsScenePositionChanges, true);
    }
}
void MainScene::selectMove(){
    //m_select_rect.setRect(QRectF(m_start_selecting,m_end_selecting));
    m_select_rect.setRect(0,0,m_end_selecting.x() - m_start_selecting.x(),m_end_selecting.y() - m_start_selecting.y());
    m_select_rect.setFlag(QGraphicsItem::ItemSendsGeometryChanges, true);
}
void MainScene::selectEnded()
{
    QPainterPath selected_path;
    selected_path.addRect(QRectF(m_start_selecting,m_end_selecting));
    this->setSelectionArea(selected_path);
    m_select_rect.setVisible(false);
    m_start_selecting = QPointF(0,0);
    m_end_selecting = QPointF(0,0);
    m_selecting = false;
    m_select_rect.setFlag(QGraphicsItem::ItemSendsScenePositionChanges, true);
}

void MainScene::keyPressEvent(QKeyEvent *event)
{
  QGraphicsScene::keyPressEvent(event);
}

void MainScene::keyReleaseEvent(QKeyEvent *event)
{
    QGraphicsScene::keyReleaseEvent(event);
}

void MainScene::mousePressEvent(QGraphicsSceneMouseEvent *event){
    if(event->buttons() == Qt::LeftButton ){
        //m_start_selecting = event->buttonDownScenePos(Qt::LeftButton);
    }
    QGraphicsScene::mousePressEvent(event);
}

void MainScene::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QGraphicsView* _parentView = dynamic_cast<QGraphicsView*>(parent());
  /*
    if(event->buttons() == Qt::LeftButton && event->modifiers() == Qt::ShiftModifier){
        QPointF mPos = event->scenePos();
        QRectF sRect = _parentView->sceneRect();
        qreal newX = sRect.x() + (m_end_selecting.x() - mPos.x());
        qreal newY = sRect.y() + (m_end_selecting.y() - mPos.y());
        sRect.setX(newX);
        sRect.setY(newY);
        _parentView->setSceneRect(sRect);
     }else if(event->buttons() == Qt::LeftButton && _parentView->viewport()->cursor().shape() != Qt::ClosedHandCursor
            && _parentView->viewport()->cursor().shape() != Qt::OpenHandCursor){
        m_end_selecting = event->scenePos();
        QGraphicsItem* tmp = this->itemAt(event->scenePos(),QTransform());
        if(m_selecting){
            selectMove();
        }else if(tmp == NULL || !tmp->flags().testFlag(QGraphicsItem::ItemIsSelectable)){
                startSelecting();
        }
    }
    */
    QGraphicsScene::mouseMoveEvent(event);
}
void MainScene::mouseReleaseEvent(QGraphicsSceneMouseEvent *event){
  /*
    if(m_selecting){
        selectEnded();
    }
    */

    QGraphicsScene::mouseReleaseEvent(event);
}

void MainScene::dragEnterEvent(QGraphicsSceneDragDropEvent *event)
{
    qDebug() << "ms dragEnter ";
    QGraphicsScene::dragEnterEvent(event);
}
void MainScene::dragMoveEvent(QGraphicsSceneDragDropEvent *event){
    qDebug() << "ms dragMove " << " X  " << event->screenPos().x() << " Y  " << event->screenPos().y();

    QGraphicsScene::dragMoveEvent(event);
}

void MainScene::dragLeaveEvent(QGraphicsSceneDragDropEvent *event)
{
    qDebug() << "ms dragLeave ";
    QGraphicsScene::dragLeaveEvent(event);
}
void MainScene::dropEvent(QGraphicsSceneDragDropEvent *event)
{
    qDebug() << "ms DROP ";
    QGraphicsScene::dropEvent(event);
}
void MainScene::drawBackground(QPainter *painter, const QRectF &rect)
{
  QColor c (Qt::black);
  painter->setPen(c);
  QPen pen = painter->pen();

  //draw not used brackground
  painter->setBrush(QBrush(Qt::lightGray));
  painter->drawRect(rect);


  //draw grid lines.
  pen.setWidthF(0.1);
  painter->setPen(pen);
  qreal _space = 5;
  qreal _width = 1000;
  qreal _height = 1000;

  /*if(d_panel){
      QRectF _rect =  d_panel->getRect();
      _width = _rect.width();
      _height = _rect.height();

    }*/

  QVector<QLineF> lines;
  for (int y= -_height; y < _height; y+=_space)
  {
      lines.append(QLineF(-_width,y, _width, y));
  }

  for (int x= -_width; x < _width; x+=_space)
  {
      lines.append(QLineF(x,-_height, x, _height));
  }
  painter->drawLines(lines);
}

void MainScene::drawForeground(QPainter *painter, const QRectF &rect)
{
  QPen pen(QColor(Qt::yellow));
  painter->setPen(pen);
  painter->drawRect(rect);
}
