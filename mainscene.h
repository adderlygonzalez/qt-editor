#ifndef MAINSCENE_H
#define MAINSCENE_H

#include<QtWidgets>
#include"items/sceneruler.h"
#include"items/documentpanel.h"


class MainScene: public QGraphicsScene
{
  public:
    MainScene(QObject *parent = 0);
    void init();
    void setDocumentPanel(DocumentPanel* panel){d_panel = panel;}
  protected:
    void keyPressEvent(QKeyEvent *event);
    void keyReleaseEvent(QKeyEvent *event);
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);

    void dragEnterEvent(QGraphicsSceneDragDropEvent *event);
    void dragMoveEvent(QGraphicsSceneDragDropEvent *event);
    void dragLeaveEvent(QGraphicsSceneDragDropEvent *event);
    void dropEvent(QGraphicsSceneDragDropEvent *event);

    void drawBackground(QPainter *painter, const QRectF &rect);
    void drawForeground(QPainter *painter, const QRectF &rect);
  public slots:

  private:
    void startSelecting();
    void selectMove();
    void selectEnded();

    DocumentPanel* d_panel;
    SceneRuler* ruler;
    QGraphicsRectItem m_select_rect;
    bool m_selecting;
    QPointF m_start_selecting;
    QPointF m_end_selecting;
};

#endif // MAINSCENE_H
