#include "mainwidget.h"
#include "ui_mainwindow.h"

MainWidget* MainWidget::_instance = NULL;

MainWidget* MainWidget::instance()
{
    MainWidget* mainW = NULL;
    if(_instance == NULL){
        mainW = new MainWidget;
    }
    return mainW;
}

MainWidget::MainWidget(QWidget *parent) :
    QMainWindow(parent),ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    init();
}

void MainWidget::init()
{
    itemBar = new ItemsBar();
    view = new MainView(this);
   // itemGrid =  new ItemGrid(this);
    toolWidget = new ToolWidget(this);
    toolWidget->setMainScene(view->getMainScene());
    setUpWidgets();

   // connect(view->scene(),&MainScene::selectionChanged,this,&MainWidget::selectedItemsChanged);
   // connect(this,&MainWidget::setSelectedItems,toolWidget,&ToolWidget::setSelectedItems);

 //   loadDocument(":/resources/default.json");

}

void MainWidget::createNew(QString name, QString docType, QString resolution)
{
  int width = 0;
  int height = 0;

  QTextStream(&resolution) >> width >> height;

  doc = new Document;
  DocumentPanel* d_panel = doc->createNew(name ,width,height,12);
  view->setUpDocument(d_panel);
}

void MainWidget::loadDocument(const char *filename)
{
  doc = new Document;
  DocumentPanel* d_panel = doc->importFromJson(filename);
  view->setUpDocument(d_panel);
}

/*
void MainWidget::prepareScene(DocumentData &sceneData)
{
    if(view){
        view->setUpScene(sceneData);
    }
}
*/

void MainWidget::setUpWidgets()
{
    this->setGeometry(0,0,851,481);
    QHBoxLayout *layout= new QHBoxLayout;

    QSplitter* splitter = new QSplitter(this);
    splitter->addWidget(itemBar);
   // splitter->addWidget(itemGrid);
    splitter->addWidget(view);
    splitter->addWidget(toolWidget);
    layout->addWidget(splitter);
    layout->setMargin(0);
    ui->centralwidget->setLayout(layout);
}

void MainWidget::selectedItemsChanged()
{
    emit this->setSelectedItems(view->scene()->selectedItems());
}
void MainWidget::createNewAccepted(NewDocumentDialog *d)
{
  if(d->ok)
    {
      createNew(d->docName,d->selectedDoc,d->selectedOption);
    }else{
      qDebug() << "Not Okay new";
    }
}

void MainWidget::keyReleaseEvent(QKeyEvent *k_evt)
{
  if(k_evt->key() == Qt::Key_Escape){
      //QApplication::quit();
    }
}

ToolWidget* MainWidget::getToolWidget()
{
    return toolWidget;
}


void MainWidget::on_actionExit_triggered()
{
    QApplication::quit();
}

void MainWidget::on_actionJson_triggered()
{
  DocumentPanel* d_panel = view->getDocument();
  QJsonObject* obj = doc->parseDocument(d_panel);
  QFileDialog* dialog= new QFileDialog(this);
  dialog->setFileMode(QFileDialog::AnyFile);
  dialog->setAcceptMode(QFileDialog::AcceptOpen);
  dialog->setDirectory(QDir::homePath());
  //dialog->selectFile("NewFile");
  QString fileName = QFileDialog::getSaveFileName(this,tr("Save as"),QDir::homePath(),tr("Json File(*.json *.JSON)"));
  qDebug() << "Selected file "<< fileName;

  if(!fileName.isEmpty()){
      doc->saveToFile(obj,fileName);
    }
}

void MainWidget::on_actionGameplay3d_triggered()
{
  DocumentPanel* d_panel = view->getDocument();
  QJsonObject* obj = doc->parseDocument(d_panel);
  QFileDialog* dialog= new QFileDialog(this);
  dialog->setFileMode(QFileDialog::AnyFile);
  dialog->setAcceptMode(QFileDialog::AcceptOpen);
  dialog->setDirectory(QDir::homePath());
  //dialog->selectFile("NewFile");
  QString fileName = QFileDialog::getSaveFileName(this,tr("Save as"),QDir::homePath(),tr("Gameplay Form(*.form *.FORM)"));
  qDebug() << "Selected file "<< fileName;
/*
  if(!fileName.isEmpty()){
      doc->saveToFile(obj,fileName);
    }*/
 QString data =  doc->exportGamePlayForm(*obj);
 QFile f_info(fileName);
 if(f_info.open(QFile::ReadWrite)){
     QTextStream o_stream;
     o_stream.setDevice(&f_info);
     o_stream << data;
   }
 if(f_info.isOpen()) f_info.close();
}

void MainWidget::on_actionPng_triggered()
{
  QFileDialog* dialog= new QFileDialog(this);
  dialog->setFileMode(QFileDialog::AnyFile);
  dialog->setAcceptMode(QFileDialog::AcceptOpen);
  dialog->setDirectory(QDir::homePath());
  //dialog->selectFile("NewFile");
  QString fileName = QFileDialog::getSaveFileName(this,tr("Save as"),QDir::homePath(),tr("Picture (*.png )"));
  qDebug() << "Selected file "<< fileName;
  if(!fileName.isEmpty())
    {
      QImage img = this->view->getSceneContent();
      //img.save(fileName);
    }
}

void MainWidget::on_actionOpen_triggered()
{
  QString fileName = QFileDialog::getOpenFileName(this,tr("Open"),QDir::homePath(),tr("Json (*.json *.JSON )"));
  qDebug() << "Save file "<< fileName;
  if(!fileName.isEmpty())
    {
      loadDocument(fileName.toStdString().c_str());
    }

}

void MainWidget::on_actionNew_triggered()
{
  NewDocumentDialog* nDialog = new  NewDocumentDialog(this);
  nDialog->setModal(true);
  nDialog->show();
  QObject::connect(nDialog,&NewDocumentDialog::createNewAccepted,this,&MainWidget::createNewAccepted);

}




