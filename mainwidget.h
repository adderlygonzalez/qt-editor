#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QtWidgets>
#include <QtUiTools>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFileDialog>
#include"items/robot.h"
#include"ui/toolwidget.h"
#include"ui/newdocumentdialog.h"
#include"mainscene.h"
#include"mainview.h"
#include"itemgrid.h"
#include"ui/itemsbar.h"
#include"document.h"

namespace Ui{
    class MainWindow;
}

/**
    This class is containing all the elements of the application.
    here it is structured the ui, and most of the connection between
    the views and toolbars.

*/

class ItemGrid;
class MainWidget : public QMainWindow
{
    Q_OBJECT
public:
    static MainWidget* instance();
    ToolWidget* getToolWidget();
    /** Init some components of the ui and the app loginc.*/
    void init();

    /** Method for preparing the scene with the data of a loaded json file or a new one. */
    //void prepareScene(DocumentData &sceneData);
    void loadDocument(const char* filename);
    void createNew(QString name,QString docType,QString resolution/* xxx*xxx */);

protected:
    void keyReleaseEvent(QKeyEvent *k_evt);
signals:
    void setSelectedItems(QList<QGraphicsItem*> items);
public slots:
    void selectedItemsChanged();
    void createNewAccepted(NewDocumentDialog*);//this would be called from NewDocumentDialog on creation.

private slots:
    void on_actionExit_triggered();

    void on_actionJson_triggered();

    void on_actionOpen_triggered();

    void on_actionNew_triggered();

    void on_actionGameplay3d_triggered();

    void on_actionPng_triggered();

private:
    Document* doc;/** document containing the info about the scene */
    MainWidget(QWidget *parent = 0);
    static MainWidget* _instance ;
    MainView* view; /** The Widget with the main scene.*/
    ItemGrid* itemGrid ; /** Item catalog widget.*/
    ToolWidget* toolWidget; /** Toolbar.*/
    ItemsBar* itemBar;
    /** Place the widgets in position. */
    void setUpWidgets();
    Ui::MainWindow *ui;
};

/**
  Document* doc =  new Document;
  QJsonParseError parsed = doc->validateJsonFile(":/resources/Doc.json");
  if(parsed.error == QJsonParseError::NoError){
      doc->printDocument();
  }
*/

#endif // MAINWIDGET_H
